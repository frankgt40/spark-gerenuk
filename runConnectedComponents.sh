#!/bin/bash

# bunch of configuration, either specify or use the default
heapSize=${1:-30g}
appName=${2:-"ConnectedComponents_$heapSize"}
numPart=${3:-320}
inputs=${4:-"$numPart /dataForSparkGerenuk/twitter_rv_25G.net /dataForSparkGerenuk/graphx/usersData"}
outputFile=${5:-"hdfs:/dataForSparkGerenuk/outputs/ConnectedComponentsResults"}
driverCores=32
num_executors=10
cmdCommons=" --master spark://zion-12:7077  --num-executors $num_executors --executor-cores $driverCores  --driver-cores $driverCores"

hdfs dfs -rm -r "$outputFile"

time ./bin/spark-submit  --name $appName $cmdCommons --driver-memory $heapSize --executor-memory $heapSize --class org.apache.spark.examples.graphx.ConnectedComponents ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar $inputs $outputFile |& tee ./outputSpark.txt

