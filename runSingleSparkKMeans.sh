#!/bin/bash

# bunch of configuration, either specify or use the default
heapSize=${1:-10g}
appName=${2:-"SparkKMeansSingleThread_$heapSize"}
inputs=${3:-"file:///lv_scratch/input-30G.csv 30 1"}
#outputFile=${4:-"/dataForSparkGerenuk/sparkKMeansResults"}
cmdCommons=" --master local[1]"
#hdfs dfs -rm -r "$outputFile"

time ./bin/spark-submit  --name \"${appName}_$heapSize\" $cmdCommons  --driver-memory $heapSize --executor-memory $heapSize --class org.apache.spark.examples.SparkKMeans  ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar $inputs  |& tee ./outputSpark.txt

