#!/bin/bash
# bunch of configuration, either specify or use the default
heapSize=${1:-10g}
appName=${2:-"ElementwiseProductExample_$heapSize"}
driverCores=32
num_executors=10
outputFiles="/dataForSparkGerenuk/EPOutput1 /dataForSparkGerenuk/EPOutput2"
inputs="/dataForSparkGerenuk/input-30G.csv 320 $outputFiles"
cmdCommons=" --master spark://zion-12:7077  --num-executors $num_executors --executor-cores $driverCores  --driver-cores $driverCores"

hdfs dfs -rm -r $outputFiles

time ./bin/spark-submit --name \"${appName}\" $cmdCommons --driver-memory $heapSize --executor-memory $heapSize --class org.apache.spark.examples.mllib.ElementwiseProductExample ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar $inputs  |& tee ./outputSpark.txt
