#!/bin/bash

# bunch of configuration, either specify or use the default
heapSize=${1:-10g}
appName=${2:-"SparkLRSingleThread_$heapSize"}
cmdCommons=" --master local[1]"
inputs="hdfs:/dataForSparkGerenuk/LR_Data 10000000 10 320" # point_num dimension partition_num

#hdfs dfs -rm -r "$outputFile"

time ./bin/spark-submit  --name \"${appName}_$heapSize\" $cmdCommons  --driver-memory $heapSize --executor-memory $heapSize --class org.apache.spark.examples.SparkLR  ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar $inputs  |& tee ./outputSpark.txt

