#!/bin/bash

# bunch of configuration, either specify or use the default
appName=${"GerenukTriangleCounting_local"}
inputs=${"/dataForSparkGerenuk/twitter_rv_small.net /dataForSparkGerenuk/graphx/usersData"}
outputFile=${"hdfs:/dataForSparkGerenuk/outputs/GerenukTriangleCountingResults"}
cmdCommons=" --master local[*]"

hdfs dfs -rm -r "$outputFile"

time ./bin/spark-submit  --name $appName $cmdCommons --class org.apache.spark.examples.graphx.GerenukTriangleCounting ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar -1 $inputs $outputFile |& tee ./outputGerenuk.txt
