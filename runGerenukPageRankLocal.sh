#!/bin/bash

# bunch of configuration, either specify or use the default
appName=${1:-"GerenukPageRank"}
inputs=${3:-"/dataForSparkGerenuk/twitter_rv_small.net 10 "}
outputFile=${4:-"hdfs:/dataForSparkGerenuk/outputs/GerenukPageRankResults"}
cmdCommons=" --master local[*] "

hdfs dfs -rm -r "$outputFile"

time ./bin/spark-submit  --name $appName $cmdCommons  --class org.apache.spark.examples.GerenukPageRank ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar $inputs $outputFile |& tee ./outputGerenuk.txt
