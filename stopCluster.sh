#!/bin/bash

# stop slaves
stop-slaves.sh spark://zion-12:7077

# stop master
stop-master.sh

# stop yarn
stop-yarn.sh

# stop hdfs
stop-dfs.sh

