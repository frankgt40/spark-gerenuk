package gerenuk;

import scala.Tuple2;

public class Gerenuk {
    public static int ABORT_ITERATION=5;
    public static boolean use_gerenuk = false;
    public static boolean noDeleteTmpFiles =false;
    public final static boolean DEBUG = false;
    public static boolean RECORD_SERIALIZATION_TIME = false;
    public static long serializationTime = 0L;
    public static long deserializationTime = 0L;
    public static boolean inSerialization = false;
    public static boolean inDeserialization = false;
    public static long fetchTime = 0L;
    public static void printSerializationDeserializationTime() {
        System.out.println("[Gerenuk] singgle thread mode.");
        System.out.println("[Gerenuk] serialization time: "+serializationTime);
        System.out.println("[Gerenuk] deserialization time: "+deserializationTime);
        System.out.println("[Gerenuk] gerenuk fetch time: "+fetchTime);
    }
    public static String mkString(Object obj) {
        if (null == obj) return null;
        if (obj instanceof byte[]) {
            byte[] array = (byte[]) obj;
            return mkByteArrayString(array);
        } if (obj instanceof char[]) {
            char[] array = (char[]) obj;
            return new String(array);
        } if (obj instanceof Tuple2) {
            return obj.toString();
        } else {
            return "";
        }
    }
    public static String mkDenseVector(double[] array) {
        double label = array[0];
        String rsl = label+",[";
        for (int i = 1; i < array.length; i++) rsl += array[i];
        rsl += "]";
        return rsl;
    }
    private static String mkByteArrayString(byte[] array) {
        String rsl = "";
        for (int i = 0; i < array.length; i++)
            rsl += (char)array[i];
        return rsl;
    }
    // solve all the hashCode problem that arrays have
    public static int hashCode(Object obj) {
        if (null == obj)
            return 0;
        else if (!(obj instanceof Object)) return (int)obj;
        else if (obj instanceof byte[]) {
            byte[] array = (byte[]) obj;
            int h = 0;
            if (h == 0 && array.length > 0) {
                for (int i = 0; i < array.length; i++) {
                    h = 31 * h + (char) hashCode(array[i]);
                }
            }
            return h;
        } else if (obj instanceof char[]) {
            char[] array = (char[]) obj;
            int h = 0;
            if (h == 0 && array.length > 0) {
                for (int i = 0; i < array.length; i++) {
                    h = 31 * h + (char) hashCode(array[i]);
                }
            }
            return h;
        } else if (obj instanceof Tuple2<?,?>) {
//            System.out.println("[Gerenuk] called TUple hashcode!");
            Tuple2<?,?> t2 = (Tuple2<?,?>) obj;
            return hashCode(t2._1) ^ hashCode(t2._2);
        } else
            return obj.hashCode();
    }
    // Call with precaution: obj1 and obj2 must be both scala.Array[Byte]
    public static boolean byteArrayEquals(Object obj1, Object obj2) {
        if (null == obj1 && null == obj2) return true;
        if (null == obj1 || null == obj2) return false;
        byte[] arr1 = (byte[]) obj1;
        byte[] arr2 = (byte[]) obj2;
        if (arr1.length != arr2.length) return false;
        for (int i = 0; i < arr1.length; i++)
            if ((char)arr1[i] != (char)arr2[i]) return false;
        return true;
    }

}
