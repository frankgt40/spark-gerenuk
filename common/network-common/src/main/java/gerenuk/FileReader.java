package gerenuk;

import java.io.File;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.EnumSet;

public class FileReader {
    File shuffleFile;
    Path path;
    MappedByteBuffer buffer = null;
    long offset;
    long length;
    void setShuffleFile(File file) {
        this.shuffleFile = file;
        path = FileSystems.getDefault().getPath(file.getPath());
    }

    public MappedByteBuffer getBuffer() {
        return buffer;
    }

    void load(long offset, long length) {
        this.offset = offset;
        this.length = length;
       // System.out.println("[Gerenuk] Entering Load " + Thread.currentThread().getId() + ", path:" + path);
        try  {
            if (null != buffer) return;
            FileChannel fileChannel = (FileChannel) Files.newByteChannel(path, EnumSet.of(StandardOpenOption.READ));
            //buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());
            buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, this.offset, this.length);
        } catch (IOException ioe) {
            System.err.println("[Gerenuk] IO exception in FileReader.load();");
        }
    }
    byte getByte(int i) {
//        System.out.println("[Gerenuk] getByte("+i+")");
        return buffer.get(i);
    }
    void test() {
        System.out.println("[Gerenuk] in FileReader.test()");
//        while (buffer.hasRemaining()) {
//            System.out.println("[Gerenuk] buffer.next: " + buffer.getChar());
//        }
        for (int i = 0; i < 10; i++)
            System.out.println("[Gerenuk] buffer[" + i + "]: " + (char)buffer.get(i));
    }
    void getBytes(byte[] dst, int l, int r) {
        buffer.get(dst, l, r);
    }

}
