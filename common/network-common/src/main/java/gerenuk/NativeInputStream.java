package gerenuk;

import com.google.common.io.ByteStreams;
import org.apache.spark.network.buffer.FileSegmentManagedBuffer;
import org.apache.spark.network.util.JavaUtils;
import sun.misc.Unsafe;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class NativeInputStream extends InputStream {
    private static int count = 0;
    private int id = 0;
    private final static int BYTE = 1;
    private long length;
//    private long address;
    private int idx;
    private long offset;
    protected char[] chars = new char[32];
//    private Unsafe us;
    private  FileReader us = new FileReader();
    public NativeInputStream(FileSegmentManagedBuffer buf) {
        File file = buf.getFile();
        /// debug start
//        try {
//            File targetFile = new File("./"+file.getName() + ".txt");
//            if (targetFile.exists())
//                targetFile.delete();
//            Files.copy(file.toPath(), targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        /// debug end
        if (null == file || !file.exists()) {
            System.err.println("[Gerenuk] buf.getFile() returns no files!");
            throw new RuntimeException("buf.getFile() returns no files!");
        }
        if (us == null) {
            us = new FileReader();
        }

        idx = 0;
        this.length = buf.getLength();
        this.offset = buf.getOffset();

        us.setShuffleFile(file);
        us.load(offset, length);
    }
    /** Reads the specified number of bytes into a new byte[]. */
    public byte[] readBytes (byte[] dst) throws EOFException {
        int length = dst.length;
        if (0 == length) throw new EOFException("[Gerenuk] array length is zero!");
//        System.arraycopy(us.getBuffer(), idx, dst, 0, length);
        for (int i = 0; i < length; i++) {
            dst[i] = getNextByte();
        }
        return dst;
    }

    public boolean readBoolean () throws EOFException {
        return getNextByte() == 1;
    }
    private void isEOF() throws EOFException {
//        if (Gerenuk.DEBUG) System.out.println("[Gerenuk] In isEOF(), id: " + id + ", idx: " + idx + ", length: " + length);
        if (idx >= length) {
            throw new EOFException("NIS ends");
        }
    }
    /** Reads a 1-9 byte long. This stream may consider such a variable length encoding request as a hint. It is not guaranteed that
     * a variable length encoding will be really used. The stream may decide to use native-sized integer representation for
     * efficiency reasons. */
    public long readLong (boolean optimizePositive) throws EOFException {
        return readVarLong(optimizePositive);
    }
    /** Reads an 8 byte long. */
    public long readLong () throws EOFException {
        return (long)getNextByte() << 56 //
                | (long)(getNextByte() & 0xFF) << 48 //
                | (long)(getNextByte() & 0xFF) << 40 //
                | (long)(getNextByte() & 0xFF) << 32 //
                | (long)(getNextByte() & 0xFF) << 24 //
                | (getNextByte() & 0xFF) << 16 //
                | (getNextByte() & 0xFF) << 8 //
                | getNextByte() & 0xFF;

    }
    public double[] readDoubles (int length) throws EOFException {
        double[] array = new double[length];
        for (int i = 0; i < length; i++)
            array[i] = readDouble();
        return array;
    }
    public double readDouble () throws EOFException {
        return Double.longBitsToDouble(readLong());
    }
    /** Bulk input of a long array. */
    public long[] readLongs (int length, boolean optimizePositive) throws EOFException {
        long[] array = new long[length];
        for (int i = 0; i < length; i++)
            array[i] = readLong(optimizePositive);
        return array;
    }
    /** Bulk input of an int array. */
    public int[] readInts (int length, boolean optimizePositive) throws EOFException {
        int[] array = new int[length];
        for (int i = 0; i < length; i++)
            array[i] = readInt(optimizePositive);
        return array;
    }
    /** Reads a 1-9 byte long. It is guaranteed that a varible length encoding will be used. */
    public long readVarLong (boolean optimizePositive) throws EOFException {
        isEOF();
        int b = getNextByte();
        long result = b & 0x7F;
        if ((b & 0x80) != 0) {
            b = getNextByte();
            result |= (b & 0x7F) << 7;
            if ((b & 0x80) != 0) {
                b = getNextByte();
                result |= (b & 0x7F) << 14;
                if ((b & 0x80) != 0) {
                    b = getNextByte();
                    result |= (b & 0x7F) << 21;
                    if ((b & 0x80) != 0) {
                        b = getNextByte();
                        result |= (long)(b & 0x7F) << 28;
                        if ((b & 0x80) != 0) {
                            b = getNextByte();
                            result |= (long)(b & 0x7F) << 35;
                            if ((b & 0x80) != 0) {
                                b = getNextByte();
                                result |= (long)(b & 0x7F) << 42;
                                if ((b & 0x80) != 0) {
                                    b = getNextByte();
                                    result |= (long)(b & 0x7F) << 49;
                                    if ((b & 0x80) != 0) {
                                        b = getNextByte();
                                        result |= (long)b << 56;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!optimizePositive) result = (result >>> 1) ^ -(result & 1);
        return result;
    }
    /** Reads a 1-5 byte int. This stream may consider such a variable length encoding request as a hint. It is not guaranteed that
     * a variable length encoding will be really used. The stream may decide to use native-sized integer representation for
     * efficiency reasons. **/
    public int readInt (boolean optimizePositive) throws EOFException {
        int rsl =  readVarInt(optimizePositive);
        return rsl;
    }
    private Unsafe getUnsafe() throws IllegalAccessException, NoSuchFieldException {
        Field f = Unsafe.class.getDeclaredField("theUnsafe");
        f.setAccessible(true);
        return (Unsafe) f.get(null);
    }
    public void fill(File file) throws IOException {
        FileInputStream is = null;
        try {
            is = new FileInputStream(file);
            ByteStreams.skipFully(is, offset);
        } catch (IOException e) {
            try {
                if (is != null) {
                    long size = file.length();
                    throw new IOException("Error in reading " + this + " (actual file length " + size + ")",
                            e);
                }
            } catch (IOException ignored) {
                // ignore
            } finally {
                JavaUtils.closeQuietly(is);
            }
            throw new IOException("Error in opening " + this, e);
        } catch (RuntimeException e) {
            JavaUtils.closeQuietly(is);
            throw e;
        }
    }
    public void close() {
        us = null;
        try {
            super.close();
        } catch (IOException e) {
            System.err.println("IO exception in NativeInputStream.close(), stack: ");
            e.printStackTrace();
        }
    }

    public byte get(int idx) throws NoSuchFieldException, IllegalAccessException {
        return us.getByte(idx);
    }

    public byte getNextByte() throws EOFException {
        try {
            if (Gerenuk.DEBUG) System.out.println("[Gerenuk] in getNextByte(), idx: " + idx);
            return us.getByte(idx++);//this.buffer[this.idx++];
        } catch (Exception e) {
            throw new EOFException();
        }
    }
    public long length() {
        return length;
    }

//    public void free() throws NoSuchFieldException, IllegalAccessException {
//        getUnsafe().freeMemory(address);
//        this.us.
//    }

    @Override
    public int read() throws IOException {
        return 0;
    }

    public static void copyFileUsingStream(File source, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }
    public String toString() {
        String rsl = "";
        for (int i = (int)offset; i < length; i++) {
            byte b = us.getByte(i);
            rsl += (char)b + ",";
        }
        return rsl;
    }
    public void print() {
        String rsl = toString();
    }
    public byte[] readChars() throws  EOFException {
        isEOF();
        int b = getNextByte();
        if ((b & 0x80) == 0) return readAsciiChars(); // ASCII.
        // Null, empty, or UTF8.
        int charCount = readUtf8Length(b);
        switch (charCount) {
            case 0:
                return null;
            case 1:
                return new byte[0];
        }
        charCount--;
        if (chars.length < charCount) chars = new char[charCount];
        return readUtf8Chars(charCount);
    }
    public char readChar () throws EOFException {
        return (char)(((getNextByte() & 0xFF) << 8) | (getNextByte() & 0xFF));
    }
    public char[] readChars(int length) throws EOFException {
        char[] array = new char[length];
        for (int i = 0; i < length; i++)
            array[i] = readChar();
        return array;
    }
    public String readString() throws EOFException {
//        if (Gerenuk.DEBUG) System.out.println("[Gerenuk] in NativeInputStream.readString(), idx: " + idx + ", try to read byte!");
        isEOF();
        int b = getNextByte(); //us.getByte(address + idx++);
//        if (Gerenuk.DEBUG) System.out.println("[Gerenuk] NativeInputStream.readString() b(int): " + b + ", b(char): " + (char)b);
        if ((b & 0x80) == 0) return readAscii(); // ASCII.
        // Null, empty, or UTF8.
        int charCount = readUtf8Length(b);
//        if (Gerenuk.DEBUG) System.out.println("[Gerenuk] NativeInputStream.readString(), charCount: " + charCount);
        switch (charCount) {
            case 0:
                return null;
            case 1:
                return "";
        }
        charCount--;
        if (chars.length < charCount) chars = new char[charCount];
        return readUtf8(charCount);
//        return new String(chars, 0, charCount);
    }
    private byte[] readUtf8Chars (int charCount) throws EOFException {
//        System.out.println("[Gerenuk] Entered readUtf8Chars()");
        isEOF();
        int b;
        int oldIdx = idx;
        while (idx < charCount) {
            b = getNextByte();//us.getByte(address + idx++);//this.buffer[this.idx++];
//            if (Gerenuk.DEBUG) System.out.println("[Gerenuk] NativeInputStream.readUtf8() b: " + b);
            if (b < 0) {
                idx--;
                break;
            }
        }
        int len = idx - oldIdx;
        byte[] rsl = new byte[len];
        getArrayFromUS(rsl, oldIdx, len);
//        System.out.println("[Gerenuk] rsl of readUtf8Chars: " + rsl.toString());
        return rsl;
    }

    private String readUtf8 (int charCount) throws EOFException {
        isEOF();
        String rsl = "";
        int i = 0;
        int b;
        while (i < charCount) {
//            if (Gerenuk.DEBUG) System.out.println("[Gerenuk] in NativeInputStream.readUtf8(), try to read byte!");
            b = getNextByte();//us.getByte(address + idx++);//this.buffer[this.idx++];
//            if (Gerenuk.DEBUG) System.out.println("[Gerenuk] NativeInputStream.readUtf8() b: " + b);
            if (b < 0) {
                idx--;
                break;
            }
            rsl += (char)b;
            i++;
        }
        return rsl;
    }

    private byte[] readAsciiChars() throws  EOFException {
        isEOF();
        int end = (int) idx;
        int start = end-1;
        int b;
        do {
            b = us.getByte(end++);
//            if (Gerenuk.DEBUG) System.out.println("[Gerenuk] NativeInputStream.readAsciiChars(), end: " + (end-1) + ",  (ID: " + id + "), b: " + b + ", b(char): " + (char)b);
        } while ((b & 0x80) == 0);
//        if (Gerenuk.DEBUG) System.out.println("[Gerenuk] NativeInputStream.readAsciiChars(), end: " + end + ",  start: " + start);
        int len = end-start;
        byte[] rsl = new byte[len];
        getArrayFromUS(rsl, start, len);
        idx = end;
        rsl[len - 1] &= 0x7F; // Mask end of ascii bit.
        return rsl;
    }
    private void getArrayFromUS(byte[] dst, int start, int len) {
        for (int i = 0; i < len; i++) {
            dst[i] = us.getByte(start+i);
        }
    }
    private String readAscii () throws EOFException {
        isEOF();
        int end = (int) idx;
        int start = end-1;
        int b;
        String value = new String();
        do {
            b = us.getByte(end++);
//            if (Gerenuk.DEBUG) System.out.println("[Gerenuk] NativeInputStream.readAscii(), end: " + end + ",  (ID: " + id + "), b: " + b + ", b(char): " + (char)b);
        } while ((b & 0x80) == 0);
        for (int i = start; i < end-1; i++) {
            value += (char)us.getByte(i);
        }
        b = us.getByte(end-1) & 0x7F; // Mask end of ascii bit.
        value += (char)b;
        idx = end;
//        if (Gerenuk.DEBUG) {
//            System.out.println("[Gerenuk] NativeInputStream.readAscii() (ID: " + id + "), last b: " + b + ", b(char): " + (char) b);
//            System.out.println("[Gerenuk] NativeInputStream.readAscii() (ID: " + id + "), idx: " + idx + ", value: " + value);
//        }
        return value;
    }

    private int readUtf8Length (int b) throws EOFException {
        isEOF();
        int result = b & 0x3F; // Mask all but first 6 bits.
        if ((b & 0x40) != 0) { // Bit 7 means another byte, bit 8 means UTF8.
            b = getNextByte();//us.getByte(address + idx++);//this.buffer[this.idx++];
            result |= (b & 0x7F) << 6;
            if ((b & 0x80) != 0) {
                b = getNextByte();//us.getByte(address + idx++);//this.buffer[this.idx++];
                result |= (b & 0x7F) << 13;
                if ((b & 0x80) != 0) {
                    b = getNextByte();//us.getByte(address + idx++);//this.buffer[this.idx++];
                    result |= (b & 0x7F) << 20;
                    if ((b & 0x80) != 0) {
                        b = getNextByte();//us.getByte(address + idx++);//this.buffer[this.idx++];
                        result |= (b & 0x7F) << 27;
                    }
                }
            }
        }
        return result;
    }


    public int readVarIntOLD(boolean optimizePositive) throws EOFException {
        isEOF();
        char b2 = (char)getNextByte();//us.getByte(address + idx++);//this.buffer[this.idx++];
        if (Gerenuk.DEBUG) System.out.println("[Gerenuk] 1.NativeInputStream.readVarInt() b2: " + b2);
        int b = b2;
        if (Gerenuk.DEBUG) System.out.println("[Gerenuk] 1.NativeInputStream.readVarInt() b: " + b);
        int result = b & 127;
        if ((b & 128) != 0) {
            //byte[] buffer = this.buffer; why?? just for saving typing? Frankgt40
            b = getNextByte();//us.getByte(address + idx++);//buffer[this.idx++];
            if (Gerenuk.DEBUG) System.out.println("[Gerenuk] NativeInputStream.readVarInt() b: " + b);
            result |= (b & 127) << 7;
            if ((b & 128) != 0) {
                b = getNextByte();//us.getByte(address + idx++);//buffer[this.idx++];
                if (Gerenuk.DEBUG) System.out.println("[Gerenuk] NativeInputStream.readVarInt() b: " + b);
                result |= (b & 127) << 14;
                if ((b & 128) != 0) {
                    b = getNextByte();//us.getByte(address + idx++);//buffer[this.idx++];
                    if (Gerenuk.DEBUG) System.out.println("[Gerenuk] NativeInputStream.readVarInt() b: " + b);
                    result |= (b & 127) << 21;
                    if ((b & 128) != 0) {
                        b = getNextByte();//us.getByte(address + idx++);//buffer[this.idx++];
                        if (Gerenuk.DEBUG) System.out.println("[Gerenuk] NativeInputStream.readVarInt() b: " + b);
                        result |= (b & 127) << 28;
                    }
                }
            }
        }
        result = optimizePositive ? result : result >>> 1 ^ -(result & 1);
        if (Gerenuk.DEBUG) System.out.println("[Gerenuk] NativeInputStream.readVarInt() --END,  result: " + result);
        return result;
    }

    public int readVarInt(boolean optimizePositive) throws EOFException {
        isEOF();
        int b = getNextByte();//us.getByte(address + idx++);//this.buffer[this.idx++];
        int result = b & 0x7F;
        if ((b & 0x80) != 0) {
            //byte[] buffer = this.buffer; why?? just for saving typing? Frankgt40
            b = getNextByte();//us.getByte(address + idx++);//buffer[this.idx++];
            result |= (b & 0x7F) << 7;
            if ((b & 0x80) != 0) {
                b = getNextByte();//us.getByte(address + idx++);//buffer[this.idx++];
                result |= (b & 0x7F) << 14;
                if ((b & 0x80) != 0) {
                    b = getNextByte();//us.getByte(address + idx++);//buffer[this.idx++];
                    result |= (b & 0x7F) << 21;
                    if ((b & 0x80) != 0) {
                        b = getNextByte();//us.getByte(address + idx++);//buffer[this.idx++];
                        result |= (b & 0x7F) << 28;
                    }
                }
            }
        }
        result = optimizePositive ? result : result >>> 1 ^ -(result & 1);
        return result;
    }
}
