#!/bin/bash
EXECUTOR_MEM=$1
EXECUTOR_CORE=$2
CASE=$3
NUM_PART=$4
spark-submit --master spark://zion-12.cs.ucla.edu:7077 --driver-memory 30g --executor-memory $EXECUTOR_MEM --executor-cores $EXECUTOR_CORE --class org.apache.spark.examples.graphx.Analytics $SPARK_HOME/examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar $CASE hdfs://zion-12.cs.ucla.edu:9000/dataForSparkGerenuk/twitter_rv_5G.net --numEPart=$NUM_PART
