#!/bin/bash
# bunch of configuration, either specify or use the default
heapSize=${1:-10g}
appName=${2:-"GerenukGBC_$heapSize"}
driverCores=32
num_executors=10
inputs="/dataForSparkGerenuk/sample_libsvm_data.txt 2"
outputFile="/dataForSparkGerenuk/myGradientBoostingClassificationModel"
cmdCommons=" --master local[1]"

hdfs dfs -rm -r "$outputFile"

time ./bin/spark-submit --name \"${appName}_$heapSize\" $cmdCommons --class org.apache.spark.examples.mllib.GerenukGradientBoostingClassificationExample  ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar $inputs $outputFile  |& tee ./outputGerenuk.txt


