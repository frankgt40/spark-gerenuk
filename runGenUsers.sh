#!/bin/bash

hdfs dfs -rm -r /dataForSparkGerenuk/graphx/usersData

./bin/spark-submit --name "GenUsersDataSmall"  --master  spark://zion-12:7077 --num-executors 10 --executor-cores 32 --executor-memory 30g --class org.apache.spark.examples.graphx.GerenukGenUsers ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar 320 /dataForSparkGerenuk/twitter_rv_25G.net /dataForSparkGerenuk/graphx/usersData |& tee ./outputSpark.txt
