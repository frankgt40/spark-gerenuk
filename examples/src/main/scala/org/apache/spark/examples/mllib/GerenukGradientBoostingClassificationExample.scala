/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples.mllib

import gerenuk.{Gerenuk, SparseVectorFunctions}
import org.apache.spark.{SparkConf, SparkContext}
// $example on$
import org.apache.spark.mllib.tree.GradientBoostedTrees
import org.apache.spark.mllib.tree.configuration.BoostingStrategy
import org.apache.spark.mllib.tree.model.GradientBoostedTreesModel
import org.apache.spark.mllib.util.MLUtils
// $example off$

object GerenukGradientBoostingClassificationExample {
  def main(args: Array[String]): Unit = {
    val inputFile = args(0)
    val partNum = args(1).toInt
    val iterNum = args(2).toInt
    val outputModelPath = args(3)
    println(s"[Gerenuk] partNum: $partNum, iterNum: $iterNum, outputModelPath: $outputModelPath")
    val conf = new SparkConf()//.setAppName("GerenukGradientBoostedTreesClassificationExample")
    val sc = new SparkContext(conf)
    // $example on$
    // Load and parse the data file.
    // GKDDD
    val loadedData = MLUtils.loadLibSVMFileGerenukSparse(sc, inputFile, partNum)
    val numFeature = loadedData._1
    val data = loadedData._2
    // GKUU
    // Split the data into training and test sets (30% held out for testing)
    val splits = data.randomSplit(Array(0.7, 0.3))
    val (trainingData, testData) = (splits(0), splits(1))

    // Train a GradientBoostedTrees model.
    // The defaultParams for Classification use LogLoss by default.
    val boostingStrategy = BoostingStrategy.defaultParams("Classification")
    boostingStrategy.numIterations = iterNum //3 // Note: Use more iterations in practice.
    boostingStrategy.treeStrategy.numClasses = 2
    boostingStrategy.treeStrategy.maxDepth = 5
    // Empty categoricalFeaturesInfo indicates all features are continuous.
    boostingStrategy.treeStrategy.categoricalFeaturesInfo = Map[Int, Int]()
    // GKDDD
    val model = GradientBoostedTrees.trainGerenukSparse(trainingData, numFeature, boostingStrategy)
    // GKU
    // Evaluate model on test instances and compute test error
    val labelAndPreds = testData.map { point =>
      // GKDD
      val prediction = model.predictGerenukSparse(point, numFeature)
      // GKUU
      // Gerenuk
      val rsl = new Array[Double](2)
      rsl(0) = SparseVectorFunctions.getLabel(point)
      rsl(1) = prediction
      rsl
    }
    val testErr = labelAndPreds.filter(r => r(0) != r(1)).count.toDouble / testData.count()
    println(s"Test Error = $testErr")
    println(s"Learned classification GBT model:\n ${model.toDebugString}")

    // Save and load model
    model.save(sc, outputModelPath)
    val sameModel = GradientBoostedTreesModel.load(sc,
      outputModelPath)
    // $example off$
    if (Gerenuk.RECORD_SERIALIZATION_TIME)
      Gerenuk.printSerializationDeserializationTime()
    sc.stop()
  }
}
// scalastyle:on println


