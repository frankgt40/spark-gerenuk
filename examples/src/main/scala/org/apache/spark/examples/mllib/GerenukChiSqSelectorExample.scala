/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples.mllib

import gerenuk.{DenseVectorFunctions, Gerenuk, SparseVectorFunctions}
import org.apache.spark.{SparkConf, SparkContext}
// $example on$
import org.apache.spark.mllib.feature.ChiSqSelector
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.util.MLUtils
// $example off$

object GerenukChiSqSelectorExample {

  def main(args: Array[String]): Unit = {

    val conf = new SparkConf()//.setAppName("ChiSqSelectorExample")
    val sc = new SparkContext(conf)

    val filePath = args(0)
    val featureNum = args(1).toInt
    val outputFile = args(2)
    // $example on$
    // Load some data in libsvm format
    // GD
    val dataLoaded = MLUtils.loadLibSVMFileGerenukSparse(sc, filePath, 320) //"data/mllib/sample_libsvm_data.txt")
    val featuresNum = dataLoaded._1
    val data = dataLoaded._2
    // Discretize data in 16 equal bins since ChiSqSelector requires categorical features
    // Even though features are doubles, the ChiSqSelector treats each unique value as a category
    val discretizedData = data.map { lp =>
      // GD
      val newArray = new Array[Double](featuresNum+1) // with label
      newArray(0) = SparseVectorFunctions.getLabel(lp)
      val size = SparseVectorFunctions.getSize(lp)
      for (i <- 1 to size-1) newArray(i) = (lp(i)/16).floor
      newArray
//      lp.tail.tail.map(x => (x/16).floor) // two tails ignore label and the last index
//      LabeledPoint(lp.label, Vectors.dense(lp.features.toArray.map { x => (x / 16).floor }))
    }
    // Create ChiSqSelector that will select top 50 of 692 features
    val selector = new ChiSqSelector(featureNum) //50)
    // Create ChiSqSelector model (selecting features)
    // GD
    val transformer = selector.fitGerenukDense(discretizedData)
    // Filter the top 50 features from each feature vector
    val filteredData = discretizedData.map { lp =>
      // GD
      transformer.transformGerenukDense(lp)
      // GU
    }
    // $example off$

//    println("filtered data: ")
//    filteredData.foreach(x => println(DenseVectorFunctions.print(x)))
    filteredData.saveAsTextFile(outputFile)
    if (Gerenuk.RECORD_SERIALIZATION_TIME)
      Gerenuk.printSerializationDeserializationTime()
    sc.stop()
  }

}
// scalastyle:on println
