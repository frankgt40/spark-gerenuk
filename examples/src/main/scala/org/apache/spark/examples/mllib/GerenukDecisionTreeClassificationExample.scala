/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples.mllib

import gerenuk.SparseVectorFunctions
import org.apache.spark.{SparkConf, SparkContext}
// $example on$
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.mllib.tree.model.DecisionTreeModel
import org.apache.spark.mllib.util.MLUtils
// $example off$

object GerenukDecisionTreeClassificationExample {

  def main(args: Array[String]): Unit = {

    val inputFile = args(0)
    val partNum = args(1).toInt
    val outputModelPath = args(2)
    val conf = new SparkConf().setAppName("GerenukDecisionTreeClassificationExample")
    val sc = new SparkContext(conf)

    // $example on$
    // Load and parse the data file.
    val dataLoaded = MLUtils.loadLibSVMFileGerenukSparse(sc, inputFile, partNum)
    val numFeature = dataLoaded._1
    val data = dataLoaded._2
    // Split the data into training and test sets (30% held out for testing)
    val splits = data.randomSplit(Array(0.7, 0.3))
    val (trainingData, testData) = (splits(0), splits(1))

    // Train a DecisionTree model.
    //  Empty categoricalFeaturesInfo indicates all features are continuous.
    val numClasses = 2
    val categoricalFeaturesInfo = Map[Int, Int]()
    val impurity = "gini"
    val maxDepth = 5
    val maxBins = 32

    // GKDD
    val model = DecisionTree.trainClassifierGerenukSparse(trainingData, numFeature, numClasses, categoricalFeaturesInfo,
      impurity, maxDepth, maxBins)
    // GKU

    // Evaluate model on test instances and compute test error
    val labelAndPreds = testData.map { point =>
      val prediction = model.predictGerenukSparse(point, numFeature)
      val rsl = new Array[Double](2)
      rsl(0) = SparseVectorFunctions.getLabel(point)
      rsl(1) = prediction
      rsl
    }
    val testErr = labelAndPreds.filter(r => r(0) != r(1)).count().toDouble / testData.count()
    println(s"Test Error = $testErr")
    println(s"Learned classification tree model:\n ${model.toDebugString}")

    // Save and load model
    model.save(sc, outputModelPath)
    val sameModel = DecisionTreeModel.load(sc, outputModelPath)
    // $example off$

    sc.stop()
  }
}
// scalastyle:on println
