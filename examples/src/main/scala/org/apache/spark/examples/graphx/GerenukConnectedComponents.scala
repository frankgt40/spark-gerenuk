/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples.graphx

// $example on$
import gerenuk.Gerenuk
import org.apache.spark.graphx.GraphLoader
import org.apache.spark.rdd.RDD
// $example off$
import org.apache.spark.sql.SparkSession


object GerenukConnectedComponents {
  def main(args: Array[String]): Unit = {
    if (args.length < 4) {
      System.err.println("Usage: SparkKMeans <partition_num> <edge_file> <username_file> <output_file>")
      System.exit(1)
    }
    gerenuk.Gerenuk.use_gerenuk = true
    // Creates a SparkSession.
    val spark = SparkSession
      .builder
      .getOrCreate()
    val sc = spark.sparkContext

    val part = args(0).toInt
    // $example on$
    // Load the graph as in the PageRank example
    val graph = GraphLoader.edgeListFile(sc, args(1), numEdgePartitions = part)
    // Find the connected components
    val cc = graph.connectedComponents().vertices
//    println(s"[Gerenuk] cc.toDebugString ${cc.toDebugString}")
    // Join the connected components with the usernames
    val users = sc.textFile(args(2)).map { line =>
      val fields = line.split(",")
      (fields(0).toLong, fields(1).getBytes)
    }
//    println("[Gerenuk] users: ")
//    users.foreach(x => println(s"[Gerenuk] user: $x"))
//    println("[Gerenuk] cc: ")
//    cc.foreach(x => println(s"[Gerenuk] ccPair: $x"))
    val ccByUsernameJoined = users.join(cc)
//    ccByUsernameJoined.foreach(t => {
//      println(s"[Gerenuk] pair: (${t._1}, (${Gerenuk.mkString(t._2._1)}, ${t._2._2}))")
//    })
    val ccByUsername = ccByUsernameJoined.asInstanceOf[RDD[(Long, (Array[Byte], Long))]].map {
      case (id, (username, cc)) => (username, cc)
    }
    // Print the result
    ccByUsername.saveAsTextFile(args(3))
    // $example off$
    spark.stop()
  }
}
// scalastyle:on println
