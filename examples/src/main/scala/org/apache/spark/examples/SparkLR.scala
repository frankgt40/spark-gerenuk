/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples

import java.util.Random

import scala.math.exp
import breeze.linalg.{DenseVector, Vector}
import gerenuk.Gerenuk
import org.apache.spark.sql.SparkSession

/**
 * Logistic regression based classification.
 * Usage: SparkLR [partitions]
 *
 * This is an example implementation for learning how to use Spark. For more conventional use,
 * please refer to org.apache.spark.ml.classification.LogisticRegression.
 */
object SparkLR {
  val R = 0.7  // Scaling factor
  val ITERATIONS = 10
  val rand = new Random(42)

  case class DataPoint(x: Vector[Double], y: Double)

  def generateData(n: Int, d: Int): Array[DataPoint] = {
    def generatePoint(i: Int): DataPoint = {
      val y = if (i % 2 == 0) -1 else 1
      val x = DenseVector.fill(d) {rand.nextGaussian + y * R}
      DataPoint(x, y)
    }
    Array.tabulate(n)(generatePoint)
  }

  def showWarning() {
    System.err.println(
      """WARN: This is a naive implementation of Logistic Regression and is given as an example!
        |Please use org.apache.spark.ml.classification.LogisticRegression
        |for more conventional use.
      """.stripMargin)
  }

  def main(args: Array[String]) {

    showWarning()

    val filePath = args(0)
    val N = args(1).toInt  // Number of data points
    val D = args(2).toInt  // Number of dimensions
    val spark = SparkSession
      .builder
      .appName("SparkLR")
      .getOrCreate()
    Gerenuk.use_gerenuk = false
    val numSlices = args(3).toInt
    val points = spark.sparkContext.textFile(filePath, numSlices).map{ line =>
      val elements = line.split(',')
      val y = elements.last.toDouble
      val arr = new Array[Double](elements.length-1)
      for (i <- 0 until elements.length-1)
        arr(i) = elements(i).toDouble
      val x = DenseVector[Double](arr)
      DataPoint(x, y)
    }//.parallelize(generateData(N, D), numSlices).cache()

    // Initialize w to a random value
    val w = DenseVector.fill(D) {2 * rand.nextDouble - 1}
//    println(s"Initial w: $w")

    for (i <- 1 to ITERATIONS) {
      println(s"On iteration $i")
      val gradient = points.map { p =>
        p.x * (1 / (1 + exp(-p.y * (w.dot(p.x)))) - 1) * p.y
      }.reduce(_ + _)
      w -= gradient
    }

    println(s"Final w: $w")
    if (Gerenuk.RECORD_SERIALIZATION_TIME)
      Gerenuk.printSerializationDeserializationTime()
    spark.stop()
  }
}
// scalastyle:on println
