/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples

import java.util.Random

import breeze.linalg.{DenseVector, Vector}
import gerenuk.Gerenuk
import org.apache.spark.sql.SparkSession

import scala.math.exp

/**
 * Logistic regression based classification.
 * Usage: SparkLR [partitions]
 *
 * This is an example implementation for learning how to use Spark. For more conventional use,
 * please refer to org.apache.spark.ml.classification.LogisticRegression.
 */
object GerenukLR {
  val R = 0.7  // Scaling factor
  val ITERATIONS = 10
  val rand = new Random(42)

  case class DataPoint(x: Array[Double], y: Double)

  def generateData(n: Int, d: Int): Array[Array[Double]] = {
    def generatePoint(i: Int): Array[Double] = {
      val y = if (i % 2 == 0) -1 else 1
      val x = Array.fill[Double](d+1)(rand.nextGaussian + y * R)//DenseVector.fill(D) {rand.nextGaussian + y * R}
      x(d) = y
      x
    }
    Array.tabulate(n)(generatePoint)
  }

  def showWarning() {
    System.err.println(
      """WARN: This is a naive implementation of Logistic Regression and is given as an example!
        |Please use org.apache.spark.ml.classification.LogisticRegression
        |for more conventional use.
      """.stripMargin)
  }

  def onePointMulti(p: Array[Double], v: Double): Array[Double] = p.map(_*v)

  def onePointDot(p: Array[Double], p2:Array[Double]): Double = {
    def onePointDotHelper(acc: Double): Double = {
      if (p.isEmpty) acc
      else acc + (p.head + p2.head)
    }
    onePointDotHelper(0)
  }
  def pointOp(p: Array[Double], p2: Array[Double], fun33: (Double, Double) => Double): Array[Double] = {
    for (i <- 0 to p.length-1) p(i) = p(i) + p2(i)
    p
  }
  def arrToString(v: Array[Double]): String = v.mkString("[", ",", "]")
  def main(args: Array[String]) {

    showWarning()

    val filePath = args(0)
    val N = args(1).toInt  // Number of data points
    val D = args(2).toInt  // Number of dimensions
    Gerenuk.use_gerenuk = true

    val spark = SparkSession
      .builder
      .appName("GerenukLR")
      .getOrCreate()

    val numSlices = args(3).toInt
    val points = spark.sparkContext.textFile(filePath, numSlices).map{ line =>
      val elements = line.split(',')
      val arr = new Array[Double](elements.length)
      for (i <- 0 until elements.length)
        arr(i) = elements(i).toDouble
      arr
    } //.parallelize(generateData(N, D), numSlices).cache()

    // Initialize w to a random value
    val w = Array.fill(D) {2 * rand.nextDouble - 1}
//    println(s"Initial w: $w")

    for (i <- 1 to ITERATIONS) {
      println(s"On iteration $i")
      val gradient = points.map { p =>
        val x = p.slice(0, D)
        val y = p(D)
        val wDotX = onePointDot(x, w)
        val term = (1 / (1 + exp(-y * wDotX)) - 1)
        onePointMulti(onePointMulti(x, term), y)
      }.reduce( (p1, p2) => pointOp(p1, p2, _ + _))
      pointOp(w, gradient, _ - _)
//      w -= gradient
    }

    println(s"Final w: ${arrToString(w)}")

    if (Gerenuk.RECORD_SERIALIZATION_TIME)
      Gerenuk.printSerializationDeserializationTime()

    spark.stop()
  }
}
// scalastyle:on println
