/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples

import breeze.linalg.{DenseVector, Vector, squaredDistance}
import gerenuk.Gerenuk
import org.apache.spark.sql.SparkSession

/**
 * K-means clustering.
 *
 * This is an example implementation for learning how to use Spark. For more conventional use,
 * please refer to org.apache.spark.ml.clustering.KMeans.
 */
object AbortGerenukKMeans {

//  def parseVector(line: String): Vector[Double] = {
//    DenseVector(line.split(',').map(_.toDouble))
//  }
  def parseVector(line: String): Array[Double] = {
    line.split(',').map(_.toDouble)
  }

  def squaredDistance[T, U](p: Array[Double], center: Array[Double]): Double = {
    var squaredDistance = 0.0
    for (i <- 0 until p.length) {
      val a = p(i)
      val b = center(i)
      val score = a - b
      squaredDistance += (score * score)
    }
    squaredDistance
  }
  def closestPoint(p: Array[Double], centers: Array[Array[Double]]): Int = {
    var bestIndex = 0
    var closest = Double.PositiveInfinity

    for (i <- 0 until centers.length) {
      val tempDist = squaredDistance(p, centers(i))
      if (tempDist < closest) {
        closest = tempDist
        bestIndex = i
      }
    }

    bestIndex
  }

  def showWarning() {
    System.err.println(
      """WARN: This is a naive implementation of KMeans Clustering and is given as an example!
        |Please use org.apache.spark.ml.clustering.KMeans
        |for more conventional use.
      """.stripMargin)
  }
  def add2Points(p1: Array[Double], p2: Array[Double]): Array[Double] = {
    val rsl: Array[Double] = new Array(p1.length)
    for (i <- 0 until p1.length) {
      rsl(i) = p1(i) + p2(i)
    }
    rsl
  }
  def multiPointByAnNumber(p: Array[Double], scale: Double): Array[Double] = {
    val rsl: Array[Double] = new Array(p.length)
    for (i <- 0 until p.length)
      rsl(i) = p(i) * scale
    rsl
  }
//  def printClosest(t: (Int, (Array[Double], Int))): String = s"[Gerenuk] closest: (${t._1}, (${t._2._1}, ${t._2._2}))"

  def printKPoints(array: Array[Array[Double]]): String = {
    array.map( v => s"(${v(0)}, ${v(1)}, ${v(2)})").reduce( (a1, a2) => s"$a1 \n $a2")
  }


  def parseVector2(line: String): Vector[Double] = {
    DenseVector(line.split(',').map(_.toDouble))
  }

  def closestPoint2(p: Vector[Double], centers: Array[breeze.linalg.Vector[Double]]): Int = {
    var bestIndex = 0
    var closest = Double.PositiveInfinity

    for (i <- 0 until centers.length) {
      val tempDist = breeze.linalg.squaredDistance(p, centers(i))
      if (tempDist < closest) {
        closest = tempDist
        bestIndex = i
      }
    }

    bestIndex
  }
  def main(args: Array[String]) {

    if (args.length < 3) {
      System.err.println("Usage: SparkKMeans <file> <k> <convergeDist>")
      System.exit(1)
    }

//    Gerenuk.use_gerenuk = true
     Gerenuk.use_gerenuk = false
    showWarning()

    val spark = SparkSession
      .builder
//      .appName("SparkKMeans")
      .getOrCreate()

    val lines = spark.read.textFile(args(0)).rdd
    val data = lines.map(parseVector _).cache()
    val K = args(1).toInt
    val convergeDist = args(2).toDouble

    val kPoints = data.takeSample(withReplacement = false, K, 42)
    var tempDist = 3.0

    var iterationNum = 1

    while(tempDist > convergeDist) {
      if (Gerenuk.ABORT_ITERATION == iterationNum) {
        // simulate abort logic here
        // transform to DenseVector
        val kPoints2 = kPoints.map(arr => breeze.linalg.DenseVector(arr).asInstanceOf[Vector[Double]])

        val closest = data.map (p => {
          val pVector = breeze.linalg.DenseVector(p).asInstanceOf[Vector[Double]]
          (closestPoint2(pVector, kPoints2), (pVector, 1))
        })
        val pointStats = closest.reduceByKey{case ((p1, c1), (p2, c2)) => (p1 + p2, c1 + c2)}
        val newPoints = pointStats.map {pair =>
          (pair._1, pair._2._1 * (1.0 / pair._2._2))}.collectAsMap()

        tempDist = 0.0
        for (i <- 0 until K) {
          tempDist += breeze.linalg.squaredDistance(kPoints2(i), newPoints(i))
        }
        // Transform back to Array[Double]
        for (newP <- newPoints) {
          kPoints(newP._1) = newP._2.toArray
        }
      } else {
        val closest = data.map (p => (closestPoint(p, kPoints), (p, 1)))
        val pointStats = closest.reduceByKey{case ((p1, c1), (p2, c2)) => (add2Points(p1, p2), c1 + c2)}
        val newPoints = pointStats.map {pair =>
          (pair._1, multiPointByAnNumber(pair._2._1, (1.0 / pair._2._2)))}.collectAsMap()
        tempDist = 0.0
        for (i <- 0 until K) {
          tempDist += squaredDistance(kPoints(i), newPoints(i))
        }
        for (newP <- newPoints) {
          kPoints(newP._1) = newP._2
        }
      }
      iterationNum += 1
      println(s"Finished iteration (delta = $tempDist)")
    }

    println("Final centers:")
//    kPoints.foreach(println)
    println(printKPoints(kPoints))

    if (Gerenuk.RECORD_SERIALIZATION_TIME)
      Gerenuk.printSerializationDeserializationTime()
    spark.stop()
  }
  def printNewPoints(map: scala.collection.Map[Int, Array[Double]]): String = {
    def printVector(v: Array[Double], acc: String): String = {
      if (v isEmpty) acc
      else if (acc isEmpty) printVector(v.tail, s"${acc}${v.head}")
      else printVector(v.tail, s"${acc}, ${v.head}")
    }
    def printEachEntry(key: Int): String = s"[Gerenuk] map: [$key => ${printVector(map(key), "(")}"
    def printAllEntries(keySet: scala.collection.Set[Int], acc: String): String = {
      if (keySet isEmpty) acc
      else printAllEntries(keySet.tail, s"${acc}\n${printEachEntry(keySet.head)}")
    }
    printAllEntries(map.keySet, "")
  }
}
// scalastyle:on println
