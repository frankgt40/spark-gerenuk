/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples

import gerenuk.Gerenuk
import org.apache.spark.sql.SparkSession

/**
 * Computes the PageRank of URLs from an input file. Input file should
 * be in format of:
 * URL         neighbor URL
 * URL         neighbor URL
 * URL         neighbor URL
 * ...
 * where URL and their neighbors are separated by space(s).
 *
 * This is an example implementation for learning how to use Spark. For more conventional use,
 * please refer to org.apache.spark.graphx.lib.PageRank
 *
 * Example Usage:
 * {{{
 * bin/run-example SparkPageRank data/mllib/pagerank_data.txt 10
 * }}}
 */
object GerenukPageRankString {

  def showWarning() {
    System.err.println(
      """WARN: This is a naive implementation of PageRank and is given as an example!
        |Please use the PageRank implementation found in org.apache.spark.graphx.lib.PageRank
        |for more conventional use.
      """.stripMargin)
  }
//  def printVK(kv: Any): String = {
//    if (!kv.isInstanceOf[(Array[Byte], Iterable[Array[Byte]])]) kv.toString
//    else {
//      val t2 = kv.asInstanceOf[(Array[Byte], Iterable[Array[Byte]])]
//      val k = t2._1
//      val v = t2._2
//      val kStr = if (k.isInstanceOf[Array[Byte]]) Gerenuk.mkString(k) else k.toString
//      s"($kStr, ${printV(v)})"
//    }
//  }
//  def printV(v: Iterable[Array[Byte]]): String = {
//    def printVHelper(arr: Iterable[Array[Byte]], acc: String): String = {
//      if (arr.isEmpty) acc + ")"
//      else if (acc.length == 1) {
//        printVHelper(arr.tail, s"${acc}${Gerenuk.mkString(arr.head)}")
//      } else {
//        printVHelper(arr.tail, s"$acc, ${Gerenuk.mkString(arr.head)}")
//      }
//    }
//    if (v.isInstanceOf[Iterable[Array[Byte]]]) {
//      printVHelper(v.asInstanceOf[Iterable[Array[Byte]]], "(")
//    } else v.toString
//  }

  def main(args: Array[String]) {
    if (args.length < 3) {
      System.err.println("Usage: SparkPageRank <input_file> <iter> <output_file>")
      System.exit(1)
    }

    Gerenuk.use_gerenuk = false
    showWarning()

    val spark = SparkSession
      .builder
//      .appName("GerenukSparkPageRank")
      .getOrCreate()

    val iters = args(1).toInt
    val lines = spark.read.textFile(args(0)).rdd
//    println(s"GerenukPageRank[iteration#: $iters]")
    // start
    val links = lines.filter(str => !str.startsWith("#")).distinct().map{ s =>
      val parts = s.split("\\s+")
      (parts(0), parts(1))
    }
      // GD2
      .groupByKeyPageRankString()
      .cache()
    var ranks = links.mapValues(v => 1.0)

    for (i <- 1 to iters) {
//      println(s"Iteration#: $i")
      val joins = links.join(ranks)
      val contribs = joins.values.flatMap{ case (urls, rank) =>
        val size = urls.length
        urls.map(url => (url, rank / size))
      }
      ranks = contribs.reduceByKey(_ + _).mapValues(0.15 + 0.85 * _)
    }
    ranks.saveAsTextFile(args(2))
    spark.stop()
  }
//  def printItr(arr: Iterable[Array[Byte]]): String = {
//    def printItrHelper(arr2: Iterable[Array[Byte]], acc: String): String = {
//      if (arr2.isEmpty) s"$acc)"
//      else if (acc.length == 1) printItrHelper(arr2.tail, s"$acc${Gerenuk.mkString(arr2.head)}")
//      else printItrHelper(arr2.tail, s"$acc, ${Gerenuk.mkString(arr2.head)}")
//    }
//    printItrHelper(arr, "(")
//  }
//  def printJoin(joinedT2: (Array[Byte], (Iterable[Array[Byte]], Double))): String = {
//    s"[Gerenuk] printJoin: (${Gerenuk.mkString(joinedT2._1)}, (${printItr(joinedT2._2._1)}, ${joinedT2._2._2})); joinedT2._2._1's size: ${joinedT2._2._1.size}"
//  }
//  def printContribs(cbs: (Array[Byte], Double)): String  = {
//    s"[Gerenuk] printContribs: (${Gerenuk.mkString(cbs._1)}, ${cbs._2})"
//  }
//  def printContribsReduceByKey(cbs: (Array[Byte], Double)): String  = {
//    s"[Gerenuk] printContribsReduceByKey: (${Gerenuk.mkString(cbs._1)}, ${cbs._2})"
//  }
}
// scalastyle:on println
