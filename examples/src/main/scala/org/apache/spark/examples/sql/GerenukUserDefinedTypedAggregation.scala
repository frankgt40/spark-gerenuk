/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.spark.examples.sql

// $example on:typed_custom_aggregation$
import org.apache.spark.sql.expressions.Aggregator
import org.apache.spark.sql.{Encoder, Encoders, SparkSession}
// $example off:typed_custom_aggregation$

object GerenukUserDefinedTypedAggregation {

  /**
    * Gerenuk, results will be stored in the first array
    * @param first
    * @param firstStart
    * @param second
    * @param secondStart
    */
  def addTwoLong(first: Array[Byte], firstStart: Int, second: Array[Byte], secondStart: Int): Unit = {
    val v1 = getDouble(first, firstStart)
    val v2 = getDouble(second, secondStart)
    setADouble(v1+v2, first, firstStart)
  }
  def setADouble(value: Double, array: Array[Byte], start: Int): Unit = {
    //    fillDataByLength(value, DOUBLE_SIZE, array, start)
    val valueL = java.lang.Double.doubleToLongBits(value)
    array(start) = (valueL >>> 56).asInstanceOf[Byte]
    array(start+1) = (valueL >>> 48).asInstanceOf[Byte]
    array(start+2) = (valueL >>> 40).asInstanceOf[Byte]
    array(start+3) = (valueL >>> 32).asInstanceOf[Byte]
    array(start+4) = (valueL >>> 24).asInstanceOf[Byte]
    array(start+5) = (valueL >>> 16).asInstanceOf[Byte]
    array(start+6) = (valueL >>> 8).asInstanceOf[Byte]
    array(start+7) = (valueL).asInstanceOf[Byte]
  }
  def getDouble(array: Array[Byte], start: Int): Double = {
    val rsl = ((array(start)).asInstanceOf[Long] << 56) | ((array(start+1).asInstanceOf[Long] & 0xFF) << 48) | ((array(start+2).asInstanceOf[Long] & 0xFF) << 40) | ((array(start+3).asInstanceOf[Long] & 0xFF) << 32) | ((array(start+4).asInstanceOf[Long] & 0xFF) << 24) | ((array(start+5).asInstanceOf[Long] & 0xFF) << 16) | ((array(start+6).asInstanceOf[Long] & 0xFF) << 8) | (array(start+7).asInstanceOf[Long] & 0xFF)
    java.lang.Double.longBitsToDouble(rsl)
  }
  // $example on:typed_custom_aggregation$
//  case class Employee(name: String, salary: Long)
//  case class Average(var sum: Long, var count: Long)
  // Employee: Array[Byte] : |Long (8 Bytes)|String (Bytes)| Note: String length can be calculated by subtracting 8 bytes
  // Average: Array[Byte]: |Long (8 Bytes) | Long (8 Bytes)|

  object MyAverage extends Aggregator[Array[Byte], Array[Byte], Double] {
    // A zero value for this aggregation. Should satisfy the property that any b + zero = b
    def zero: Array[Byte] = new Array[Byte](16) // Average
    // Combine two values to produce a new value. For performance, the function may modify `buffer`
    // and return it instead of constructing a new object
    def reduce(buffer: Array[Byte], employee: Array[Byte]): Array[Byte] = {
      addTwoLong(buffer, 0, employee, 0)
      val count = getDouble(buffer, 8)
      setADouble(count+1, buffer, 8)
      buffer
    }
    // Merge two intermediate values
    def merge(b1: Array[Byte], b2: Array[Byte]): Array[Byte] = {
      val s1 = getDouble(b1, 0)
      val s2 = getDouble(b2, 0)
      val c1 = getDouble(b1, 8)
      val c2 = getDouble(b2, 8)
      setADouble(s1+s2, b1, 0)
      setADouble(c1+c2, b1, 8)
      b1
    }
    // Transform the output of the reduction
    def finish(reduction: Array[Byte]): Double = getDouble(reduction, 0) / getDouble(reduction, 8)
    // Specifies the Encoder for the intermediate value type
    def bufferEncoder: Encoder[Array[Byte]] = Encoders.BINARY
    // Specifies the Encoder for the final output value type
    def outputEncoder: Encoder[Double] = Encoders.scalaDouble
  }
  // $example off:typed_custom_aggregation$

  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .appName("Spark SQL user-defined Datasets aggregation example")
      .getOrCreate()

    import spark.implicits._

    // $example on:typed_custom_aggregation$
    val js = spark.read.json("examples/src/main/resources/employees.json")
    val ds = js.as[Array[Byte]]
    ds.show()
    // +-------+------+
    // |   name|salary|
    // +-------+------+
    // |Michael|  3000|
    // |   Andy|  4500|
    // | Justin|  3500|
    // |  Berta|  4000|
    // +-------+------+

    // Convert the function to a `TypedColumn` and give it a name
    val averageSalary = MyAverage.toColumn.name("average_salary")
    val result = ds.select(averageSalary)
    result.show()
    // +--------------+
    // |average_salary|
    // +--------------+
    // |        3750.0|
    // +--------------+
    // $example off:typed_custom_aggregation$

    spark.stop()
  }

}
