/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples

import org.apache.spark.sql.SparkSession

import scala.collection.mutable
import scala.util.Random

/**
 * Transitive closure on a graph.
 */
object GerenukTC {
  val rand = new Random(42)

  def generateGraph(numEdges: Int, numVertices: Int): Array[Int] = {
    val edges = new Array[Int](numEdges)
    var i = 0
    while (edges.length < numEdges) {
      val from = rand.nextInt(numVertices)
      val to = rand.nextInt(numVertices)
      if (from != to) {
        edges(i) = from
        edges(i+1) = to
        i = i+2
      }
    }
    edges
  }

  def main(args: Array[String]) {
    val spark = SparkSession
      .builder
      .appName("SparkTC")
      .getOrCreate()
    val slices = args(0).toInt
    val numEdges = args(1).toInt
    val numVertices = args(2).toInt
    var tc = spark.sparkContext.parallelize(generateGraph(numEdges, numVertices), slices).cache()

    // Linear transitive closure: each round grows paths by one edge,
    // by joining the graph's edges with the already-discovered paths.
    // e.g. join the path (y, z) from the TC with the edge (x, y) from
    // the graph to obtain the path (x, z).

    // Because join() joins on keys, the edges are stored in reversed order.
//    val edges = tc.map(x => (x._2, x._1))
//
//    // This join is iterated until a fixed point is reached.
//    var oldCount = 0L
//    var nextCount = tc.count()
//    do {
//      oldCount = nextCount
//      // Perform the join, obtaining an RDD of (y, (z, x)) pairs,
//      // then project the result to obtain the new (x, z) paths.
//      tc.aggregate(edges)()
//      tc = tc.union(tc.join(edges).map(x => (x._2._2, x._2._1))).distinct().cache()
//      nextCount = tc.count()
//    } while (nextCount != oldCount)
//
//    println(s"TC has ${tc.count()} edges.")
    spark.stop()
  }
}
// scalastyle:on println
