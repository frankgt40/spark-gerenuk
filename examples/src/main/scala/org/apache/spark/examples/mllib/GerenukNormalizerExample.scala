/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples.mllib

import gerenuk.SparseVectorFunctions
import org.apache.spark.{SparkConf, SparkContext}
// $example on$
import org.apache.spark.mllib.feature.Normalizer
import org.apache.spark.mllib.util.MLUtils
// $example off$

object GerenukNormalizerExample {

  def main(args: Array[String]): Unit = {

//    val conf = new SparkConf()//.setAppName("NormalizerExample")
//    val sc = new SparkContext(conf)
//
//    val filePath = args(0)
//    val partNum = args(1).toInt
//    // $example on$
//    val dataLoaded = MLUtils.loadLibSVMFileGerenukSparse(sc, filePath, partNum)
//    val data = dataLoaded._2
//
//    val normalizer1 = new Normalizer()
//    val normalizer2 = new Normalizer(p = Double.PositiveInfinity)
//
//    // Each sample in data1 will be normalized using $L^2$ norm.
//    // GD
//    val data1 = data.map(x => (SparseVectorFunctions.getLabel(x), normalizer1.transform(x)))
//
//    // Each sample in data2 will be normalized using $L^\infty$ norm.
//    val data2 = data.map(x => (SparseVectorFunctions.getLabel(x), normalizer2.transform(x)))
//    // $example off$
//
//    println("data1: ")
//    data1.foreach(x => println(x))
//
//    println("data2: ")
//    data2.foreach(x => println(x))
//
//    sc.stop()
  }
}
// scalastyle:on println
