/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples

import gerenuk.Gerenuk
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.util.collection.CompactBuffer

/**
 * Computes the PageRank of URLs from an input file. Input file should
 * be in format of:
 * URL         neighbor URL
 * URL         neighbor URL
 * URL         neighbor URL
 * ...
 * where URL and their neighbors are separated by space(s).
 *
 * This is an example implementation for learning how to use Spark. For more conventional use,
 * please refer to org.apache.spark.graphx.lib.PageRank
 *
 * Example Usage:
 * {{{
 * bin/run-example SparkPageRank data/mllib/pagerank_data.txt 10
 * }}}
 */
object Abort4GerenukPageRankString {

  def showWarning() {
    System.err.println(
      """WARN: This is a naive implementation of PageRank and is given as an example!
        |Please use the PageRank implementation found in org.apache.spark.graphx.lib.PageRank
        |for more conventional use.
      """.stripMargin)
  }
  def arrayToCompactBuffer(array: Array[String]): Iterable[String] = {
    val buff = CompactBuffer[String]
    array.foreach(x => buff+=x)
    buff
  }
  def compactBufferToArray(buff: Iterable[String]): Array[String] = {
    buff.toArray
  }

  def main(args: Array[String]) {
    if (args.length < 3) {
      System.err.println("Usage: SparkPageRank <input_file> <iter> <output_file>")
      System.exit(1)
    }

    Gerenuk.use_gerenuk = false
    showWarning()

    var abortIterNumList = List(2,4,6,8,10,12,14,16)
    var abortNumber = args(2).toInt

    val spark = SparkSession
      .builder
      .getOrCreate()

    val iters = args(1).toInt
    val lines = spark.read.textFile(args(0)).rdd
    // start
    val links = lines.filter(str => !str.startsWith("#")).distinct().map{ s =>
      val parts = s.split("\\s+")
      (parts(0), parts(1))
    }.groupByKeyPageRankString().cache()
    var ranks = links.mapValues(v => 1.0)



    var links3: RDD[(String, Array[String])] = links
    ///////////////////////// iter 1////////////////////
    val joins1 = links3.join(ranks)
    val contribs1 = joins1.values.flatMap{ case (urls, rank) =>
      val size = urls.length
      urls.map(url => (url, rank / size))
    }
    ranks = contribs1.reduceByKey(_ + _).mapValues(0.15 + 0.85 * _)
    ///////////////////////// iter 2////////////////////
    val linksObj2 = links3.map{ case(str, arr) => (str, arrayToCompactBuffer(arr))}
    val joins2 = linksObj2.join(ranks)
    val contribs2 = joins2.values.flatMap{ case (urls, rank) =>
      val size = urls.size
      urls.map(url => (url, rank / size))
    }
    // transform back to Array[String]
    ranks = contribs2.reduceByKey(_ + _).mapValues(0.15 + 0.85 * _)
    links3 = linksObj2.map(x => (x._1, compactBufferToArray(x._2)))
    ///////////////////////// iter 3////////////////////
    val joins3 = links3.join(ranks)
    val contribs3 = joins3.values.flatMap{ case (urls, rank) =>
      val size = urls.length
      urls.map(url => (url, rank / size))
    }
    ranks = contribs3.reduceByKey(_ + _).mapValues(0.15 + 0.85 * _)
    ///////////////////////// iter 4////////////////////
    val linksObj4 = links3.map{ case(str, arr) => (str, arrayToCompactBuffer(arr))}
    val joins4 = linksObj4.join(ranks)
    val contribs4 = joins4.values.flatMap{ case (urls, rank) =>
      val size = urls.size
      urls.map(url => (url, rank / size))
    }
    // transform back to Array[String]
    ranks = contribs4.reduceByKey(_ + _).mapValues(0.15 + 0.85 * _)
    links3 = linksObj4.map(x => (x._1, compactBufferToArray(x._2)))
    ///////////////////////// iter 5////////////////////
    val joins5 = links3.join(ranks)
    val contribs5 = joins5.values.flatMap{ case (urls, rank) =>
      val size = urls.length
      urls.map(url => (url, rank / size))
    }
    ranks = contribs5.reduceByKey(_ + _).mapValues(0.15 + 0.85 * _)
    ///////////////////////// iter 6////////////////////
    val linksObj6 = links3.map{ case(str, arr) => (str, arrayToCompactBuffer(arr))}
    val joins6 = linksObj6.join(ranks)
    val contribs6 = joins6.values.flatMap{ case (urls, rank) =>
      val size = urls.size
      urls.map(url => (url, rank / size))
    }
    // transform back to Array[String]
    ranks = contribs6.reduceByKey(_ + _).mapValues(0.15 + 0.85 * _)
    links3 = linksObj6.map(x => (x._1, compactBufferToArray(x._2)))
    ///////////////////////// iter 7////////////////////
    val joins7 = links3.join(ranks)
    val contribs7 = joins7.values.flatMap{ case (urls, rank) =>
      val size = urls.length
      urls.map(url => (url, rank / size))
    }
    ranks = contribs7.reduceByKey(_ + _).mapValues(0.15 + 0.85 * _)
    ///////////////////////// iter 8////////////////////
    val linksObj8 = links3.map{ case(str, arr) => (str, arrayToCompactBuffer(arr))}
    val joins8 = linksObj8.join(ranks)
    val contribs8 = joins8.values.flatMap{ case (urls, rank) =>
      val size = urls.size
      urls.map(url => (url, rank / size))
    }
    // transform back to Array[String]
    ranks = contribs8.reduceByKey(_ + _).mapValues(0.15 + 0.85 * _)
    links3 = linksObj8.map(x => (x._1, compactBufferToArray(x._2)))


    for (i <- 9 to iters) {
        val joins = links3.join(ranks)
        val contribs = joins.values.flatMap{ case (urls, rank) =>
          val size = urls.length
          urls.map(url => (url, rank / size))
        }
        ranks = contribs.reduceByKey(_ + _).mapValues(0.15 + 0.85 * _)
    }
    ranks.saveAsTextFile(args(3))
    spark.stop()
  }
}
// scalastyle:on println
