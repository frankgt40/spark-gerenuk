/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples.mllib

import org.apache.spark.{SparkConf, SparkContext}
// $example on$
import org.apache.spark.mllib.feature.ElementwiseProduct
import org.apache.spark.mllib.linalg.Vectors
// $example off$

object GerenukElementwiseProductExample {

  def main(args: Array[String]): Unit = {

    val conf = new SparkConf()//.setAppName("ElementwiseProductExample")
    val sc = new SparkContext(conf)

    val filePath = args(0)
    val partNum = args(1).toInt
    val outputFile1 = args(2)
    val outpoutFile2 = args(3)
    // $example on$
    // Create some vector data; also works for sparse vectors
    val data = sc.textFile(filePath, partNum).map(line => {
      line.split(',').map(x => x.toDouble)
    })

    val transformingVector = Vectors.dense(0.0, 1.0, 2.0)
    val transformer = new ElementwiseProduct(transformingVector)

    // Batch transform and per-row transform give the same results:
    // GD
    val transformedData = transformer.transformGerenukDense(data)
    val transformedData2 = data.map(x => transformer.transformGerenukDense(x))
    // $example off$

//    println("transformedData: ")
    transformedData.saveAsTextFile(outputFile1)//.foreach(x => println(x))

//    println("transformedData2: ")
    transformedData2.saveAsTextFile(outpoutFile2)//foreach(x => println(x))

    sc.stop()
  }
}
// scalastyle:on println
