/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples

import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}

object SparkPageRankTungsten {


  case class Edge(src: String, dest: String)


  def main(args: Array[String]) {
    val inputFilePath = args(0)
    val itrNum = args(1).toInt
    val outputFilePath = args(2)
    val spark = SparkSession
      .builder
      .getOrCreate()
//    val edgesDF = spark.read.textFile(inputFilePath)
    import spark.implicits._
    val edgesDF = spark.read.textFile(inputFilePath).filter(str => !str.startsWith("#")).map{ line=>
      val part = line.split("\\s+")
      Edge(part(0), part(1))
    }.distinct().toDF()

    val edgeAndDegreesDF = computeDegree(edgesDF)
    var edgeDegreePageRankDF = edgeAndDegreesDF.withColumn("pagerank", lit(1.0))

    for (i <- Range(0, itrNum)) {
      edgeDegreePageRankDF.cache()
      println("Iteration " + i)
      // compute the delta
      val reducedDeltasDF = computeDeltas(edgeDegreePageRankDF)
      // compute the new PR
      edgeDegreePageRankDF = updatePageRanks(edgeDegreePageRankDF, reducedDeltasDF)
    }
    val res = edgeDegreePageRankDF
      .select("src", "pagerank")
      .distinct()
    res.write.csv(outputFilePath)
  }

  def computeDegree(edgesDF: DataFrame): DataFrame = {
    val edgesDegreeDF = edgesDF
      .groupBy("src")
      .agg(count("src").as("degree"))

    val edgeAndDegreesDF = edgesDF.join(edgesDegreeDF, "src")

    edgeAndDegreesDF
  }

  def computeDeltas(edgeDegreePageRankRDD: DataFrame): DataFrame = {
    edgeDegreePageRankRDD
      .withColumn("delta", udf(computeDelta _).apply(col("pagerank"), col("degree")))
      .groupBy("dest")
      .agg(sum("delta").as("delta"))
      .withColumnRenamed("dest", "vertex")
  }

  def computeDelta(pagerank: Double, degree: Long): Double = pagerank / degree

  def updatePageRanks(edgeDegreePageRankDF: DataFrame, reducedDeltasDF: DataFrame): DataFrame = {
    val joinDF = edgeDegreePageRankDF
      .drop("pagerank")
      .join(reducedDeltasDF, edgeDegreePageRankDF("src") === reducedDeltasDF("vertex"), "left_outer")
      .drop("vertex")

    joinDF.withColumn("pagerank", udf(calcPageRank _).apply(col("src"), col("dest"), col("degree"), col("delta")))
      .drop("delta")
  }

  def calcPageRank(src: String, dest: String, degree: Long, delta: Any): Double = {
    delta match {
      case delta: Double => 1.0 + delta
      case null => 1.0
    }
  }
}
