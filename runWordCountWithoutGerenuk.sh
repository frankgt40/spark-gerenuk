#!/bin/bash

./bin/spark-submit --verbose --class org.apache.spark.examples.SparkWordCount --master local[*] ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar ./data/mergedFile100M |& tee ./output.txt
