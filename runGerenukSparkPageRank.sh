#!/bin/bash

hdfs dfs -rm -r  /dataForSparkGerenuk/SparkPageRankResults

time ./bin/spark-submit --name "GerenukSparkPageRank" --verbose --class org.apache.spark.examples.GerenukSparkPageRank --master local[2] ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar /dataForSparkGerenuk/twitter_rv_5mb.net 2 /dataForSparkGerenuk/SparkPageRankResults |& tee ./outputGerenuk.txt

#./bin/spark-submit --verbose --class org.apache.spark.examples.GerenukSparkPageRank --master local[*] ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar  |& tee ./outputGerenuk.txt
