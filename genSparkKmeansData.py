#!/usr/bin/env python
# coding: utf-8

# In[1]:


from __future__ import print_function

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.datasets.samples_generator import make_blobs
from pyspark import SparkContext
from pyspark.ml.clustering import KMeans
from pyspark.ml.feature import VectorAssembler
from pyspark.sql import SQLContext

get_ipython().run_line_magic('matplotlib', 'inline')

# print (pyspark.__version__)


# In[ ]:


n_samples=1000000
oneM=n_samples*2
oneG=oneM*1000
thirtyG=oneG*30
n_features=3
X, y = make_blobs(n_samples=thirtyG, centers=10, n_features=n_features, random_state=42)

# add a row index as a string
pddf = pd.DataFrame(X, columns=['x', 'y', 'z'])
# pddf['id'] = 'row'+pddf.index.astype(str)

#move it first (left)
cols = list(pddf)
# cols.insert(0, cols.pop(cols.index('id')))
pddf = pddf.loc[:, cols]
pddf.head()

# save the ndarray as a csv file
pddf.to_csv('input.csv', index=False)


# In[4]:


# threedee = plt.figure(figsize=(12,10)).gca(projection='3d')
# threedee.scatter(X[:,0], X[:,1], X[:,2], c=y)
# threedee.set_xlabel('x')
# threedee.set_ylabel('y')
# threedee.set_zlabel('z')
# plt.show()


# In[ ]:




