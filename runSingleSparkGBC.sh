#!/bin/bash
# bunch of configuration, either specify or use the default
heapSize=${1:-10g}
appName=${2:-"SparkGBCSingleThread_$heapSize"}
inputs="file:///lv_scratch/HIGGS3 320 5"
outputFile="file:///lv_scratch/myGradientBoostingClassificationModel"
cmdCommons=" --master local[1]"

hdfs dfs -rm -r "$outputFile"

time ./bin/spark-submit --name \"${appName}_$heapSize\" $cmdCommons --driver-memory $heapSize --executor-memory $heapSize --class org.apache.spark.examples.mllib.GradientBoostingClassificationExample ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar $inputs $outputFile  |& tee ./outputSpark.txt
