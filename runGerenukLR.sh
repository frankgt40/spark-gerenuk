#!/bin/bash

# bunch of configuration, either specify or use the default
heapSize=${1:-10g}
appName=${2:-"GerenukLR_$heapSize"}
driverCores=32
num_executors=10
cmdCommons=" --master spark://zion-12:7077  --num-executors $num_executors --executor-cores $driverCores  --driver-cores $driverCores"
inputs="hdfs:/dataForSparkGerenuk/LR_Data2 10000000 10 320" # point_num dimension partition_num

#hdfs dfs -rm -r "$outputFile"

time ./bin/spark-submit  --name \"${appName}_$heapSize\" $cmdCommons  --driver-memory $heapSize --executor-memory $heapSize --class org.apache.spark.examples.GerenukLR  ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar $inputs |& tee ./outputGerenuk.txt

