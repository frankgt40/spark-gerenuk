#/bin/bash


mvn clean
echo "maven cleaned..."

rm -f ./hs_err_pid*.log
echo "log files removed..."

rm -f ./output.txt
echo "output.txt cleaned..."

rm -f ./*~
echo "All cleaned!"
