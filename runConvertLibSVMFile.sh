#!/bin/bash
# bunch of configuration, either specify or use the default
heapSize=${1:-10g}
appName=${2:-"ConvertLibSVMFIle_$heapSize"}
driverCores=32
num_executors=10
inputs="/dataForSparkGerenuk/HIGGS3 320"
outputFile="/dataForSparkGerenuk/HIGGS3_Long"
cmdCommons=" --master spark://zion-12:7077  --num-executors $num_executors --executor-cores $driverCores  --driver-cores $driverCores"

hdfs dfs -rm -r "$outputFile"

time ./bin/spark-submit --name \"${appName}_$heapSize\" $cmdCommons --driver-memory $heapSize --executor-memory $heapSize --class org.apache.spark.examples.mllib.ConvertLibSVMFile ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar $inputs $outputFile  |& tee ./outputSpark.txt
