#!/bin/bash

hdfs dfs -rm -r /dataForSparkGerenuk/graphx/usersDataSmall

./bin/spark-submit --name "GenUsersDataSmall"  --master local[*] --class org.apache.spark.examples.graphx.GerenukGenUsers ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar 32 /dataForSparkGerenuk/twitter_rv_small.net /dataForSparkGerenuk/graphx/usersDataSmall |& tee ./outputSpark.txt
