#!/bin/bash


#build local
if [ $# == 0 ]; 
then
    #    stop-all.sh
    ./build/zinc-0.3.9/bin/zinc -shutdown # has to stop zinc server!
    echo 'Should be in local -- bare minimum - record output.txt'
    ./dev/make-distribution.sh --name skyway-local --tgz  |& tee output.txt
else
    #same, local
    if [ $1 == "0" ];
    then
        echo 'Local build -- bare minimum - record output.txt'
	./dev/make-distribution.sh --name skyway-local --tgz |& tee output.txt
    else
        if [ $1 == "1" ];
        then
            echo 'Server build -- called from server only -- check output.txt for logs'
            ./dev/make-distribution.sh --name skyway-hadoop2.6 --tgz -Psparkr -Phadoop-2.6 -Phive -Phive-thriftserver -Pmesos -Pyarn |& tee output.txt
        else
            if [ $1 == "2" ];
            then
                ./dev/make-distribution.sh --name skyway-nohadoop --tgz -Psparkr -Phive -Phive-thriftserver -Pmesos -Pyarn |& tee output.txt
                
            fi

        fi

    fi
fi
