#!/bin/bash
# bunch of configuration, either specify or use the default
heapSize=${1:-10g}
appName=${2:-"ChiSqSelector_$heapSize"}
outputFile="file:///lv_scratch/filteredDataSpark"
inputs="file:///lv_scratch/HIGGS3 100 $outputFile"
cmdCommons=" --master local[1]"

hdfs dfs -rm -r $outputFile

time ./bin/spark-submit --name \"${appName}\" $cmdCommons --driver-memory $heapSize --executor-memory $heapSize --class org.apache.spark.examples.mllib.ChiSqSelectorExample ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar $inputs $outputFile  |& tee ./outputSpark.txt
