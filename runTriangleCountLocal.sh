#!/bin/bash

# bunch of configuration, either specify or use the default
appName="TriangleCountring_local"
cmdCommons="--master local[*]"

hdfs dfs -rm -r hdfs:/dataForSparkGerenuk/outputs/LocalTriangleCountResults

time ./bin/spark-submit  --name $appName $cmdCommons --class org.apache.spark.examples.graphx.TriangleCounting ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar 32 /dataForSparkGerenuk/twitter_rv_small.net /dataForSparkGerenuk/graphx/usersDataSmall hdfs:/dataForSparkGerenuk/outputs/LocalTriangleCountResults |& tee ./outputSpark.txt

