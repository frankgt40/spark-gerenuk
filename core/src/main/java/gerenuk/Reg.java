package gerenuk;

import com.esotericsoftware.kryo.Kryo;
import gerenuk.parsers.*;
import scala.Tuple2;
import scala.Tuple3;

import static com.esotericsoftware.kryo.util.Util.className;

public class Reg {
    private final Class type;
    private final int id;
    private final Fetcher fetcher;
    private Parser parser;
    public Reg (Class type, int id, Fetcher fetcher) {
        if (type == null) throw new IllegalArgumentException("type cannot be null.");
        this.type = type;
        this.id = id;
        this.fetcher = fetcher;
    }
    public Class getType () {
        return type;
    }

    /** Returns the registered class ID.
     * @see Kryo#register(Class) */
    public int getId () {
        return id;
    }

    public String toString () {
        return "[" + id + ", " + className(type) + "]";
    }

    public Fetcher getFetcher() {return fetcher;}

    public Parser getParser() {
//        System.out.println("[Gerenuk] type: "+type);
        if (null == parser) {
            if (type == String.class) {
                parser = new StringBytesParser();//StringParser();
            } else if (type == int.class) {
                parser = new IntParser();
            } else if (type == boolean.class) {
                parser = new BooleanParser();
            } else if (type == Tuple2.class) {
                parser = new Tuple2Parser(fetcher);
            } else if (type == scala.Tuple2$mcJJ$sp.class) { // Tuple2$mcJJ$sp cannot be recognized by IDE, maybe I can improve this in the future
                parser = new Tuple2LongLongParser();
            } else if (type == Tuple3.class) {
                parser = new Tuple3Parser(fetcher);
            } else if (type == byte[].class) {
                parser = new ByteArrayParser();
            } else if (type == char[].class) {
                parser = new CharArrayParser();
            } else if (type == int[].class) {
                parser = new IntArrayParser();
            } else if (type == long[].class) {
                parser = new LongArrayParser();
            } else if (type == long[][].class) {
                parser = new LongArrayArrayParser(fetcher);
            } else if (type == char[][].class) {
                parser = new CharArrayArrayParser();
            } else if (type == long.class) {
                parser = new LongParser();
            } else if (type == double.class) {
                parser = new DoubleParser();
            } else if (type == double[].class) {
                parser = new DoubleArrayParser();
            } else {
                throw new UnsupportedOperationException("[Gerenuk] Some parser is not implemented! Type is: " + type);
            }
        }
        return parser;
    }
}
