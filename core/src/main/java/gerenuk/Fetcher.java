    package gerenuk;

    import com.esotericsoftware.kryo.Kryo;
    import com.esotericsoftware.kryo.ReferenceResolver;
    import com.esotericsoftware.kryo.Registration;
    import com.esotericsoftware.kryo.StreamFactory;
    import com.esotericsoftware.kryo.io.Input;
    import com.esotericsoftware.kryo.util.IdentityMap;
    import com.esotericsoftware.kryo.util.ObjectMap;

    import java.io.EOFException;
    import java.io.InputStream;
    import java.lang.reflect.InvocationHandler;
    import java.lang.reflect.Proxy;
    import java.util.ConcurrentModificationException;
    import java.util.EnumSet;

    import static com.esotericsoftware.kryo.util.Util.className;
    import static com.esotericsoftware.minlog.Log.DEBUG;
    import static com.esotericsoftware.minlog.Log.debug;

    public class Fetcher {
        //    protected final ObjectMap<Class, Reg> classToReg = new ObjectMap();
        private final GClassResolver classResolver = new GClassResolver();
        private final boolean DEBUG = true;
        private int depth = 0;
        private int maxDepth = 2147483647;
        private volatile Thread thread;
//        private boolean references = true;
//        private Object readObject;
        private Kryo kryo = null;
        private NativeInputStream nis;
        public Fetcher(Kryo kryo, InputStream nis) {
            this.kryo = kryo;
            classResolver.setFetcher(this);
            this.nis = (NativeInputStream)nis;
        }
        public Kryo getKryo() { return kryo;}


        private void beginObject() {
            if (DEBUG) {
                if (this.depth == 0) {
                    this.thread = Thread.currentThread();
                } else if (this.thread != Thread.currentThread()) {
                    throw new ConcurrentModificationException("Kryo must not be accessed concurrently by multiple threads.");
                }
            }

            if (this.depth == this.maxDepth) {
                throw new GerenukFetcherException("Max depth exceeded: " + this.depth);
            } else {
                ++this.depth;
            }
        }
        public Object readClassAndObject(NativeInputStream input) throws EOFException {
            if (input == null) {
                throw new IllegalArgumentException("NativeInputStream input cannot be null.");
            } else {
//                this.beginObject();
                Reg reg = this.readClass(input);
                if (reg == null) {
                    System.out.println("[Gerenuk] reg is null!");
                    return null;
                } else {
                    Class type = reg.getType();
                    return reg.getParser().readObject(input, type); // frankgt40 work point
                }
            }
        }

        public Reg readClass(NativeInputStream input) throws EOFException {
            if (input == null) {
                throw new IllegalArgumentException("NativeInputStream input cannot be null.");
            } else {
                Reg var2;
                var2 = this.classResolver.readClass(input);
                return var2;
            }
        }
        public void reset() {
            this.classResolver.reset();
//            if (this.references) {
//                this.readObject = null;
//            }

        }
    }