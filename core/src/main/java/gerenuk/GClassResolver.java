package gerenuk;

import com.esotericsoftware.kryo.ClassResolver;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.Registration;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.util.IntMap;
import com.esotericsoftware.kryo.util.ObjectMap;
import com.esotericsoftware.kryo.util.Util;
import com.esotericsoftware.minlog.Log;

import java.io.EOFException;

import static com.esotericsoftware.kryo.util.Util.className;
import static com.esotericsoftware.kryo.util.Util.log;
import static com.esotericsoftware.minlog.Log.DEBUG;
import static com.esotericsoftware.minlog.Log.TRACE;
import static com.esotericsoftware.minlog.Log.trace;

public class GClassResolver {
    protected final IntMap<Reg> idToRegistration = new IntMap();
    protected final ObjectMap<Class, Reg> classToRegistration = new ObjectMap();
    //    static public final byte NAME = -1;
//    private int memoizedClassId = -1;
//    private Reg memoizedClassIdValue;
//    private ClassLoader classLoader;
    protected Fetcher fetcher;
//    protected IntMap<Class> nameIdToClass;
//    protected ObjectMap<String, Class> nameToClass;
//    protected final IntMap<Reg> idToRegistration = new IntMap();
//   /* public GClassResolver() {
//        classLoader = new ExecutorClassLoader();
//    }*/
//    public void setFetcher(Fetcher fetcher) {this.fetcher = fetcher;}
//    public Reg register(Reg registration) {
//        return null;
//    }
//
//    //@Override
//    public Reg registerImplicit(Class type) {
//        return null;
//    }
//
//    //@Override
//    public Reg getRegistration(Class type) {
//        return null;
//    }
//
//    //@Override
//    public Reg getRegistration(int classID) {
//        return null;
//    }
//
//    //@Override
//    public Reg writeClass(Output output, Class type) {
//        throw new UnsupportedOperationException("Should not use GClassResolver to write classes!");
//    }
//

    public void setFetcher(Fetcher fetcher) {  this.fetcher = fetcher; }
    public Reg register(Reg reg) {
        if (reg == null) {
            throw new IllegalArgumentException("reg cannot be null.");
        } else {
            if (reg.getId() != -1) {
                if (Log.TRACE) {
                    Log.trace("kryo", "Register class ID " + reg.getId() + ": " + Util.className(reg.getType()) + " (" + reg.getFetcher().getClass().getName() + ")");
                }

                this.idToRegistration.put(reg.getId(), reg);
            } else if (Log.TRACE) {
                Log.trace("kryo", "Register class name: " + Util.className(reg.getType()) + " (" + reg.getFetcher().getClass().getName() + ")");
            }

            this.classToRegistration.put(reg.getType(), reg);
            if (reg.getType().isPrimitive()) {
                this.classToRegistration.put(Util.getWrapperClass(reg.getType()), reg);
            }

            return reg;
        }
    }

    ////@Override
    public Reg readClass(NativeInputStream input) throws EOFException {
        if (Gerenuk.DEBUG)  System.out.println("[Gerenuk] GClassResolver.readClass is calling");
        int classID = input.readVarInt(true);
        if (Gerenuk.DEBUG) System.out.println("[Gerenuk] GClassResolver.readClass, classID: " + classID);
        switch(classID) {
            case 0:
                if (Log.TRACE || Log.DEBUG && fetcher.getKryo().getDepth() == 1) {
                    Util.log("Read", (Object)null);
                }

                return null;
            case 1:
                String className = input.readString();
                System.out.println("[Gerenuk]  hit readName, we have to register [className: " + className + "] in KryoSerizlizer.newKryo()");
                throw new KryoException("[Gerenuk] hit readName");
//                return null;//this.readName(input);
            default:
                if (Gerenuk.DEBUG)
                    if (fetcher == null) System.out.println("[Gerenuk] fetcher is null!");
                    else System.out.println("[Gerenuk] fetcher is NOT null!");
                Kryo k = fetcher.getKryo();
                if (Gerenuk.DEBUG)
                    if (k == null) System.out.println("[Gerenuk] Kryo is null!");
                    else System.out.println("[Gerenuk] Kryo is NOT null!");
                Registration registration = k.getRegistration(classID-2);
                if (Gerenuk.DEBUG) System.out.println("[Gerenuk] registration.getSerializer: " + registration.getSerializer());
                if (registration == null) {
                        throw new KryoException("[Gerenuk] In GClassResolver.readClass; Encountered unregistered class ID: " + (classID - 2));
                }
                Reg reg = new Reg(registration.getType(), registration.getId(), fetcher);
                if (Gerenuk.DEBUG) System.out.println("[Gerenuk] the reg is: " + reg.toString());
                return reg;
//                if (classID == this.memoizedClassId) {
//                    return this.memoizedClassIdValue;
//                } else {
//                    Registration registration = (Registration)this.idToRegistration.get(classID - 2);
//                    if (registration == null) {
//                        throw new KryoException("Encountered unregistered class ID: " + (classID - 2));
//                    } else {
//                        if (Log.TRACE) {
//                            Log.trace("kryo", "Read class " + (classID - 2) + ": " + Util.className(registration.getType()));
//                        }
//
//                        this.memoizedClassId = classID;
//                        this.memoizedClassIdValue = registration;
//                        return registration;
//                    }
//                }
        }
//        switch (classID) {
//            case Kryo.NULL:
//                if (TRACE || DEBUG) log("Read", null);
//                return null;
//            case NAME + 2: // Offset for NAME and NULL.
//                return readName(input);
//        }
//        if (classID == memoizedClassId) return memoizedClassIdValue;
//        Reg registration = idToRegistration.get(classID - 2);
//        if (registration == null) throw new KryoException("Encountered unregistered class ID: " + (classID - 2));
//        if (TRACE) trace("kryo", "Read class " + (classID - 2) + ": " + className(registration.getType()));
//        memoizedClassId = classID;
//        memoizedClassIdValue = registration;
//        return registration;
//        return null;
    }
//    protected Reg readName (Input input) {
//        int nameId = input.readVarInt(true);
//        if (nameIdToClass == null) nameIdToClass = new IntMap();
//        Class type = nameIdToClass.get(nameId);
//        if (type == null) {
//            // Only read the class name the first time encountered in object graph.
//            String className = input.readString();
//            type = getTypeByName(className);
//            if (type == null) {
//                try {
//                    type = Class.forName(className, false, kryo.getClassLoader());
//                } catch (ClassNotFoundException ex) {
//                    throw new KryoException("Unable to find class: " + className, ex);
//                }
//                if (nameToClass == null) nameToClass = new ObjectMap();
//                nameToClass.put(className, type);
//            }
//            nameIdToClass.put(nameId, type);
//            if (TRACE) trace("kryo", "Read class name: " + className);
//        } else {
//            if (TRACE) trace("kryo", "Read class name reference " + nameId + ": " + className(type));
//        }
//        return classToReg.get(type);
//    }
//
//    protected Class<?> getTypeByName(final String className) {
//        return nameToClass != null ? nameToClass.get(className) : null;
//    }

    //@Override
    public void reset() {

    }
}
