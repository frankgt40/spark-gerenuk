package gerenuk

import java.nio.ByteBuffer

import scala.collection.mutable

object SparseVectorFunctions {

  val DOUBLE_SIZE = 2 // 2 Ints, 8 bytes
  val INT_SIZE = 1 // 1 Int,  4 bytes
//  val BYTE_SIZE = 1 // Byte: 8 bits
  val HEAD_SIZE = DOUBLE_SIZE + INT_SIZE

  private var lastReturnedPos = -1

  /**
    * Gerenuk
    * Array represents a LabeledPoint: |label (Double)|length (Int)|indices (Ints)|values(Doubles)|
    * The length of indices and values should be same
    * @param size: the size of indices or values
    * @return
    */
  def newArray(label: Double, size: Int): Array[Int] = {
    val realSize = HEAD_SIZE + size + size + size // size * DOUBLE_SIZE // see the format in above comments
    val array = new Array[Int](realSize)
    fillADouble(label, array, 0) // put the label in the head of the array
    fillAInt(size, array, DOUBLE_SIZE)
    array
  }

  /**
    * Gerenuk: compute the last index from indices
    * @param array
    * @return
    */
  def lastIndex(array: Array[Int]): Int = {
    val size = getSize(array)
//    println(s"[Gerenuk] lastIndex, size: $size")
    val start = size - 1
    getInt(array, HEAD_SIZE + start)
  }
  def getSize(array: Array[Int]): Int = {
    getInt(array, DOUBLE_SIZE)
  }
  def getLabel(array: Array[Int]): Double = {
    getDouble(array, 0)
  }
  def setLabel(array: Array[Int], newLabel: Double): Array[Int] = {
    fillADouble(newLabel, array, 0)
    array
  }

  def getByte(array: Array[Byte], index: Int): Byte = array(index)
  def getInt(array: Array[Int], start: Int): Int = {
//    ByteBuffer.wrap(array.slice(start, start+INT_SIZE+1)).getInt

//    val rsl = ((array(start) & 0xFF) << 24) | ((array(start + 1) & 0xFF) << 16) | ((array(start + 2) & 0xFF) << 8) | (array(start + 3) & 0xFF)
//    println(s"[Gerenuk] getInt, start: $start, rsl: $rsl")
    array(start)
  }
  def getDouble(array: Array[Int], start: Int): Double = {

//    ByteBuffer.wrap(array.slice(start, start+DOUBLE_SIZE+1)).getDouble
//    val rsl = ((array(start)).asInstanceOf[Long] << 56) | ((array(start+1).asInstanceOf[Long] & 0xFF) << 48) | ((array(start+2).asInstanceOf[Long] & 0xFF) << 40) | ((array(start+3).asInstanceOf[Long] & 0xFF) << 32) | ((array(start+4).asInstanceOf[Long] & 0xFF) << 24) | ((array(start+5).asInstanceOf[Long] & 0xFF) << 16) | ((array(start+6).asInstanceOf[Long] & 0xFF) << 8) | (array(start+7).asInstanceOf[Long] & 0xFF)
//    println(s"[Gerenuk] getDouble, start: $start, rsl: $rsl")
    val rsl = ((array(start)).asInstanceOf[Long] << 32) | array(start+1).asInstanceOf[Long]
    java.lang.Double.longBitsToDouble(rsl)
//    rsl.toDouble
  }

  /**
    * Gerenuk
    * @param value
    * @param array
    * @param start
    * @return current index in the array
    */
  def fillAInt(value: Int, array: Array[Int], start: Int): Int = {
//    fillDataByLength(value, INT_SIZE, array, start)
    array(start) = value
    start+INT_SIZE
  }

  /**
    *
    * @param value
    * @param array
    * @param start
    * @return current index in the array
    */
  def fillADouble(value: Double, array: Array[Int], start: Int): Int = {
//    fillDataByLength(value, DOUBLE_SIZE, array, start)
    val valueL = java.lang.Double.doubleToLongBits(value)
    array(start) = (valueL >>> 32).asInstanceOf[Int]
    array(start+1) = (valueL).asInstanceOf[Int]
    start+DOUBLE_SIZE
  }

//  /**
//    * Gerenuk
//    * @param value
//    * @param size
//    * @param array
//    * @param start
//    * @return current index in the array
//    */
//  def fillDataByLength(value: Any, size: Int, array: Array[Byte], start: Int): Int = {
//    val valueArray = value.asInstanceOf[Array[Byte]]
//    for (i <- 0 to size) {
//      array(start+i) = valueArray(i)
//    }
//    start + size
//  }

  def getValueByIndexValue(point: Array[Int], indexValue: Int, numFeature: Int) : Double = {
    val offset = findIndexIndex(point, indexValue, numFeature)
    if (offset >= 0) getValue(point, offset) else 0
  }

  /**
    * Gerenuk
    * @param point
    * @param i: starting from 0 to size
    * @return
    */
  def getValue(point: Array[Int], i: Int): Double = {
    val size = getSize(point)
    if (i  >= size) 0
    else {
      val start = HEAD_SIZE + size + i + i //i*DOUBLE_SIZE
//      point.slice(start, start + DOUBLE_SIZE).asInstanceOf[Double]
      getDouble(point, start)
    }
  }
  def getIndex(point: Array[Int], i: Int): Int = {
    val size = getSize(point)
    if (i >= size || i < 0) 0
    else {
      val start = HEAD_SIZE + i
//      point.slice(start, start + INT_SIZE).asInstanceOf[Int]
      getInt(point, start)
    }
  }
  protected final def findIndexIndex(point: Array[Int], indexValue: Int, numFeature: Int) : Int = {
    val size = getSize(point)
    if (indexValue < 0 || indexValue >= numFeature)
      throw new IndexOutOfBoundsException("Index "+indexValue+" out of bounds [0,"+size+")")

    if (size == 0) {
      // empty list do nothing
      -1
    } else {
      if (indexValue > getIndex(point, size - 1)) {
        // special case for end of list - this is a big win for growing sparse arrays
        ~size
      } else {
        // regular binary search from begin to end (inclusive)
        var begin = 0
        var end = size - 1

        // Simple optimization: position i can't be after offset i.
        if (end > indexValue)
          end = indexValue

        var found = false

        var mid = (end + begin) >> 1
        // another optimization: cache the last found
        // position and restart binary search from lastReturnedPos
        // This is thread safe because writes of ints
        // are atomic on the JVM.
        // We actually don't want volatile here because
        // we want a poor-man's threadlocal...
        // be sure to make it a local though, so it doesn't
        // change from under us.
        val l = lastReturnedPos
        if (l >= 0 && l < end) {
          mid = l
        }

        // unroll the loop once. We're going to
        // check mid and mid+1, because if we're
        // iterating in order this will give us O(1) access
        val mi = getIndex(point, mid)
        if (mi == indexValue) {
          found = true
        } else if (mi > indexValue) {
          end = mid - 1
        } else {
          // mi < i
          begin = mid + 1
        }

        // a final stupid check:
        // we're frequently iterating
        // over all elements, so we should check if the next
        // pos is the right one, just to see
        if (!found && mid < end) {
          val mi = getIndex(point, mid + 1)
          if (mi == indexValue) {
            mid = mid + 1
            found = true
          } else if (mi > indexValue) {
            end = mid
          } else {
            // mi < i
            begin = mid + 2
          }
        }
        if(!found)
          mid = (end + begin) >> 1


        // pick up search.
        while (!found && begin <= end) {
          if (getIndex(point, mid) < indexValue) {
            begin = mid + 1
            mid = (end + begin) >> 1
          }
          else if (getIndex(point, mid) > indexValue) {
            end = mid - 1
            mid = (end + begin) >> 1
          }
          else
            found = true
        }



        // note: hold onto a local variable
        // because multithreading
        val result = if (found || mid < 0)
          mid
        // no match found,  insertion point
        else if (indexValue <= getIndex(point, mid))
          ~mid // Insert here (before mid)
        else
          ~(mid + 1) // Insert after mid

        lastReturnedPos = result

        result
      }
    }
  }

  def norm(vector: Array[Double], p: Double): Double = {
//    require(p >= 1.0, "To compute the p-norm of the vector, we require that you specify a p>=1. " +
//      s"You specified p=$p.")
//    val values = vector match {
//      case DenseVector(vs) => vs
//      case SparseVector(n, ids, vs) => vs
//      case v => throw new IllegalArgumentException("Do not support vector type " + v.getClass)
//    }
//    val size = values.length
//
//    if (p == 1) {
//      var sum = 0.0
//      var i = 0
//      while (i < size) {
//        sum += math.abs(values(i))
//        i += 1
//      }
//      sum
//    } else if (p == 2) {
//      var sum = 0.0
//      var i = 0
//      while (i < size) {
//        sum += values(i) * values(i)
//        i += 1
//      }
//      math.sqrt(sum)
//    } else if (p == Double.PositiveInfinity) {
//      var max = 0.0
//      var i = 0
//      while (i < size) {
//        val value = math.abs(values(i))
//        if (value > max) max = value
//        i += 1
//      }
//      max
//    } else {
//      var sum = 0.0
//      var i = 0
//      while (i < size) {
//        sum += math.pow(math.abs(values(i)), p)
//        i += 1
//      }
//      math.pow(sum, 1.0 / p)
//    }
    0.0
  }
}
