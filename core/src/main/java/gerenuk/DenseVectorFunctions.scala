package gerenuk

import org.apache.spark.broadcast.Broadcast

object DenseVectorFunctions {
  def spr(d: Double, v: Array[Double], U: Array[Double]): Array[Double] = ???

  def addEquals(U1: Array[Double], U2: Array[Double]): Array[Double] = ???

  def brzAxpy(a: Double, r: Array[Double], U: Array[Double]): Array[Double] = ???


  def dot(r: Array[Double], vbr: Broadcast[Array[Double]]): Double = ???

  /**
    * Gerenuk: array format: |label|lastIndex(use Double to store)|feature1|feature2|...
    *
    * @param label
    * @param size
    * @return
    */
  def newArray(label: Double, size: Int): Array[Double] = {
    val rsl = new Array[Double](size+2)
    rsl(0) = label
    rsl
  }
  def putADouble(value: Double, array: Array[Double], start: Int): Int = {
    array(start) = value
    start+1
  }

  def print(array: Array[Double]): String = {
    def helper(array: Array[Double], acc: String): String = {
      if (array.isEmpty) acc
      else helper(array.tail, s"$acc, ${array.head}")
    }
    helper(array, "(")+")"
  }
}
