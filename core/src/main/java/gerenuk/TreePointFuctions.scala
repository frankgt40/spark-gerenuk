package gerenuk

object TreePointFuctions {
  val DOUBLE_SIZE=2
  val INT_SIZE=1
  def newArray(numFeature: Int): Array[Int] = {
    val size = DOUBLE_SIZE + numFeature*INT_SIZE
    new Array[Int](size)
  }
  def setDouble(v: Double, array: Array[Int], start: Int): Int = {
    val vL = java.lang.Double.doubleToLongBits(v)
    array(start) = (vL >>> 32).asInstanceOf[Int]
    array(start+1) = (vL).asInstanceOf[Int]
    start + 2
  }
  def setInt(v: Int, array: Array[Int], start: Int): Int = {
    array(start) = v
    start + 1
  }
  def getFirstDouble(array: Array[Int]): Double = {
    val vl = ((array(0)).asInstanceOf[Long] << 32) | ((array(2).asInstanceOf[Long] & 0xFF))
    java.lang.Double.longBitsToDouble(vl)
  }
}
