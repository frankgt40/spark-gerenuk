package gerenuk.parsers;

import gerenuk.Fetcher;
import gerenuk.NativeInputStream;

import java.io.EOFException;

public class LongArrayArrayParser extends Parser<long[][]> {
    Fetcher fetcher = null;
    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
        return 0;
    }
    public LongArrayArrayParser(Fetcher fetcher) {
        this.fetcher = fetcher;
    }

    @Override
    public long[][] readObject(NativeInputStream input, Class<long[][]> type) throws EOFException {
        int length = input.readVarInt(true) - 1;
        if (length == 0) return null;
        long[][] rsl = new long[length][];
        for (int i = 0; i < length; i++) {
            int itemLength = input.readVarInt(true);
            if (length == 0) rsl[i] =  null;
            else {
                rsl[i] = input.readLongs(itemLength-1, false);
            }
        }
        return rsl;
    }
}
