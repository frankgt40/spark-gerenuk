package gerenuk.parsers;

import gerenuk.NativeInputStream;

import java.io.EOFException;

public class LongParser extends Parser<Long> {
    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
        return 0;
    }

    @Override
    public Long readObject(NativeInputStream input, Class<Long> type) throws EOFException {
        return input.readLong(false);
    }
}
