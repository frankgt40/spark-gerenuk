package gerenuk.parsers;

import gerenuk.NativeInputStream;
import gerenuk.parsers.Parser;

import java.io.EOFException;

public class StringParser extends Parser<String> {
    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
        input.readString();
        return 0;
    }
    @Override
    public String readObject(NativeInputStream input, Class<String> type) throws EOFException {
        return type.cast(input.readString());
    }
//    public  byte[] readChars(NativeInputStream input) {
//
//    }

}

