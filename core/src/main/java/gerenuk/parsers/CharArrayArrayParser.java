package gerenuk.parsers;

import gerenuk.NativeInputStream;

import java.io.EOFException;

public class CharArrayArrayParser extends Parser<char[][]> {
    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
        return 0;
    }

    @Override
    public char[][] readObject(NativeInputStream input, Class<char[][]> type) throws EOFException {
        int length = input.readVarInt(true) - 1;
        if (length == 0) return null;
        char[][] rsl = new char[length][];
        for (int i = 0; i < length; i++) {
            int itemLength = input.readVarInt(true);
            if (length == 0) rsl[i] =  null;
            else {
                rsl[i] = input.readChars(itemLength-1);
            }
        }
        return rsl;
    }
}
