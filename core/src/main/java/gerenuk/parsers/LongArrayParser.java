package gerenuk.parsers;

import gerenuk.NativeInputStream;

import java.io.EOFException;

public class LongArrayParser extends  Parser<long[]> {
    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
        return 0;
    }

    @Override
    public long[] readObject(NativeInputStream input, Class<long[]> type) throws EOFException {
        int length = input.readVarInt(true);
        if (length == 0) return null;
        return input.readLongs(length-1, false);
    }
}
