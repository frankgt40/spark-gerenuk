package gerenuk.parsers;

import gerenuk.NativeInputStream;
import gerenuk.parsers.Parser;

import java.io.EOFException;

public class StringBytesParser extends Parser<byte[]> {
    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
//        input.readString();
        return 0;
    }

    @Override
    public byte[] readObject(NativeInputStream input, Class<byte[]> type) throws EOFException {
        return input.readChars();
    }

}

