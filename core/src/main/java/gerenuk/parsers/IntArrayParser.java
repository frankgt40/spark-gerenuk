package gerenuk.parsers;

import gerenuk.NativeInputStream;

import java.io.EOFException;

public class IntArrayParser extends Parser<int[]> {
    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
        return 0;
    }

    @Override
    public int[] readObject(NativeInputStream input, Class<int[]> type) throws EOFException {
        int length = input.readVarInt(true);
        if (length == 0) return null;
        return input.readInts(length - 1, false);
    }
}
