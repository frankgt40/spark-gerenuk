package gerenuk.parsers;

import gerenuk.NativeInputStream;

import java.io.EOFException;

public class DoubleParser extends Parser<Double> {

    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
        return 0;
    }

    @Override
    public Double readObject(NativeInputStream input, Class<Double> type) throws EOFException {
        Double rsl = Double.longBitsToDouble(input.readLong());
//        System.out.println("[Gerenuk] DoubleParser rsl: " + rsl);
        return rsl;
    }
}
