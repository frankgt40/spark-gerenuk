package gerenuk.parsers;

import gerenuk.Fetcher;
import gerenuk.NativeInputStream;
import gerenuk.parsers.Parser;
import scala.Tuple2;

import javax.ws.rs.core.FeatureContext;
import java.io.EOFException;

public class Tuple2Parser<A,B> extends Parser<Tuple2<A, B>> {
    private Fetcher fetcher = null;
    public Tuple2Parser(Fetcher fetcher) {
        this.fetcher = fetcher;
    }
    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
        return 0;
    }

    @Override
    public Tuple2<A, B> readObject(NativeInputStream input, Class<Tuple2<A, B>> type) throws EOFException {
        A a = (A)(fetcher.readClassAndObject(input));
        B b = (B)(fetcher.readClassAndObject(input));
        if (null == a) System.out.println("[Gerenuk] a is null!");
        if (null == b) System.out.println("[Gerenuk] b is null!");
//        System.out.println("[Gerenuk] Tuple2Parser readObject, a: " + a.toString() + ", b: " + b.toString());
        return new Tuple2<A, B>(a, b);
    }
}
