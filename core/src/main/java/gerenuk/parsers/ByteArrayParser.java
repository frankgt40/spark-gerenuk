package gerenuk.parsers;

import gerenuk.NativeInputStream;

import java.io.EOFException;

public class ByteArrayParser extends Parser<byte[]> {
    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
//        input.readString();
        return 0;
    }

    @Override
    public byte[] readObject(NativeInputStream input, Class<byte[]> type) throws EOFException {
        int length = input.readVarInt(true)-1;
        if (length == 0) {
            System.out.println("[Gerenuk] ByteArrayParser.readObject, length == 0");
            return null;
        }
        byte[] rsl = new byte[length];
        return input.readBytes(rsl);
    }


}
