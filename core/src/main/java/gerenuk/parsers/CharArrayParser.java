package gerenuk.parsers;

import gerenuk.NativeInputStream;

import java.io.EOFException;

public class CharArrayParser extends Parser<char[]> {
    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
        return 0;
    }

    @Override
    public char[] readObject(NativeInputStream input, Class<char[]> type) throws EOFException {
        int length = input.readVarInt(true);
        if (length == 0) return null;
        return input.readChars(length-1);
    }
}
