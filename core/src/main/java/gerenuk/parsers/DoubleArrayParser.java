package gerenuk.parsers;

import gerenuk.NativeInputStream;

import java.io.EOFException;

public class DoubleArrayParser extends Parser<double[]> {
    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
        return 0;
    }

    @Override
    public double[] readObject(NativeInputStream input, Class<double[]> type) throws EOFException {
        int length = input.readVarInt(true);
        if (0 == length) return null;
        return input.readDoubles(length-1);
    }
}
