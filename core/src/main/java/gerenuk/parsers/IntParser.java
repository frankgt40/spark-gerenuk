package gerenuk.parsers;

import gerenuk.NativeInputStream;
import gerenuk.parsers.Parser;

import java.io.EOFException;

public class IntParser extends Parser<Integer> {
    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
        return  input.readInt(false);
    }

    @Override
    public Integer readObject(NativeInputStream input, Class<Integer> type) throws EOFException {
        return new Integer(input.readInt(false));
    }
}
