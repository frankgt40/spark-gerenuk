package gerenuk.parsers;

import gerenuk.Fetcher;
import gerenuk.NativeInputStream;
import scala.Tuple3;

import java.io.EOFException;

public class Tuple3Parser<A, B, C> extends Parser<Tuple3<A, B, C>> {
    private Fetcher fetcher = null;
    public Tuple3Parser(Fetcher fetcher) {
        this.fetcher = fetcher;
    }
    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
        return 0;
    }

    @Override
    public Tuple3<A, B, C> readObject(NativeInputStream input, Class<Tuple3<A, B, C>> type) throws EOFException {
        return new Tuple3<A, B, C>(
                (A)fetcher.readClassAndObject(input),
                (B)fetcher.readClassAndObject(input),
                (C)fetcher.readClassAndObject(input));
    }
}
