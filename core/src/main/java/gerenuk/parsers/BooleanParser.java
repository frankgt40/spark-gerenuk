package gerenuk.parsers;

import gerenuk.NativeInputStream;

import java.io.EOFException;

public class BooleanParser extends Parser<Boolean> {

    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
        return 0;
    }

    @Override
    public Boolean readObject(NativeInputStream input, Class<Boolean> type) throws EOFException {
        return input.readBoolean();
    }
}
