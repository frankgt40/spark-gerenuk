package gerenuk.parsers;

import gerenuk.NativeInputStream;

import java.io.EOFException;

public abstract class Parser<T> {
    private static long currAddress = 0;
    private static long nextAddress = 0;
    public abstract long readAndGetAddress(NativeInputStream input) throws EOFException;
    public abstract T readObject(NativeInputStream input, Class<T> type) throws EOFException;
}
