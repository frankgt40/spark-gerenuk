package gerenuk.parsers;

import gerenuk.NativeInputStream;
import scala.Tuple2;

import java.io.EOFException;

public class Tuple2LongLongParser extends Parser<Tuple2<Long, Long>> {
    @Override
    public long readAndGetAddress(NativeInputStream input) throws EOFException {
        return 0;
    }
    @Override
    public Tuple2<Long, Long> readObject(NativeInputStream input, Class<Tuple2<Long, Long>> type) throws EOFException {
        return new Tuple2<Long, Long>(input.readLong(), input.readLong());
    }
}
