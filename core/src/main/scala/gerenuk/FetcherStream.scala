package gerenuk



import java.io.{Closeable, EOFException, IOException, InputStream}
import java.time.{Duration, Instant}

import scala.reflect.ClassTag


class FetcherStream(val nis: InputStream, var fetcher: Fetcher) extends Closeable {

  private val this.nis: NativeInputStream = nis.asInstanceOf[NativeInputStream]

  def getFetcher: Fetcher = fetcher
  def readObject[T: ClassTag](): T = {
    val rsl = fetcher.readClassAndObject(nis.asInstanceOf[NativeInputStream])
    rsl.asInstanceOf[T]
  }
  /** Reads the object representing the key of a key-value pair. */
  def readKey[T: ClassTag](): T = readObject[T]()
  /** Reads the object representing the value of a key-value pair. */
  def readValue[T: ClassTag](): T = readObject[T]()


  def asKeyValueIterator(): Iterator[(Any, Any)] = new GNextIterator[(Any, Any)] {
    override def getNext: (Any, Any) = {
      try {
        if (Gerenuk.RECORD_SERIALIZATION_TIME) {
          import java.time.Instant
          import java.time.Duration
          val start = Instant.now
          val rsl = (readKey[Any](), readValue[Any]())
          val finish = Instant.now
          val timeElapsed = Duration.between(start, finish).toMillis // million seconds
          Gerenuk.fetchTime += timeElapsed
          rsl
        } else {
          (readKey[Any](), readValue[Any]())
        }
      } catch {
        case eof: EOFException =>
          finished = true
          null
      }
    }

    override def close(): Unit = {
      try
        nis.close()
      catch {
        case e: IOException =>
          System.err.println("[Gerenuk] FetcherStream.asKeyValueIterator, NextIterator.close() has an IO exception")
          e.printStackTrace()
      }
    }
  }

  override def close(): Unit = {
    nis.close()
  }
}
