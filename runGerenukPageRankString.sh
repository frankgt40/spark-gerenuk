#!/bin/bash

# bunch of configuration, either specify or use the default
heapSize=${1:-10g}
appName=${2:-"GerenukPageRankString"}
inputs=${3:-"hdfs:/dataForSparkGerenuk/lj 42"}
outputFile=${4:-"hdfs:/dataForSparkGerenuk/outputs/GerenukPageRankResults"}
driverCores=32
num_executors=10
cmdCommons=" --master spark://zion-12:7077  --num-executors $num_executors --executor-cores $driverCores  --driver-cores $driverCores"

hdfs dfs -rm -r "$outputFile"

time ./bin/spark-submit  --name $appName $cmdCommons --driver-memory $heapSize --executor-memory $heapSize --class org.apache.spark.examples.GerenukPageRankString ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar $inputs $outputFile |& tee ./outputGerenuk.txt
