#!/bin/bash

# bunch of configuration, either specify or use the default
heapSize=${1:-10g}
appName=${2:-"SparkPageRankSingleThread_$heapSize"}
inputs=${3:-"file:///lv_scratch/lj 42"}
outputFile=${4:-"file:///lv_scratch/SparkPageRankResults"}
cmdCommons=" --master local[1]"

hdfs dfs -rm -r  "$outputFile"

time ./bin/spark-submit --name $appName $cmdCommons --driver-memory $heapSize --executor-memory $heapSize --class org.apache.spark.examples.SparkPageRank ./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar  $inputs $outputFile |& tee ./outputSpark.txt
