#!/bin/bash

# start hdfs
start-dfs.sh

## start yarn
#start-yarn.sh

# start master
start-master.sh

# start slaves
start-slaves.sh
