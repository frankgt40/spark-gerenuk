#!/bin/bash

timeFile="./time.txt"
echo -n "" > $timeFile

SPARK="Spark"
GERENUK="Gerenuk"
appNum=4
driverCores=32
num_executors=10
heapSizeList=("15g" "13g" "10g") #12g" "10g" "7g" ) # "5g")

appNames=("ConnectedComponents"  "SparkKMeans"  "SparkPageRank" "TriangleCounting" ) # "SparkWordCount")
#inputsList=("/dataForSparkGerenuk/twitter_rv_25G.net /dataForSparkGerenuk/graphx/usersData" "/dataForSparkGerenuk/input-30G.csv 10 2" "/dataForSparkGerenuk/twitter_rv_25G.net 10" "/dataForSparkGerenuk/twitter_rv_25G.net /dataForSparkGerenuk/graphx/usersData" ) #"/dataForSparkGerenuk/twitter_rv_100m.net")
inputsList=("/dataForSparkGerenuk/twitter_rv_10G.net /dataForSparkGerenuk/graphx/usersData" "/dataForSparkGerenuk/input-30G.csv 10 2" "/dataForSparkGerenuk/twitter_rv_10G.net 10" "/dataForSparkGerenuk/twitter_rv_10G.net /dataForSparkGerenuk/graphx/usersData" ) #"/dataForSparkGerenuk/twitter_rv_100m.net")

for ((i = 0; i < $appNum; i++)); 
do
    appName=${appNames[i]}
    input="${inputsList[i]}"
    sparkJar="./examples/target/scala-2.11/jars/spark-examples_2.11-2.4.0-SNAPSHOT.jar"
    if [[ "$appName" == *"$SPARK"* ]]; then
	gerenukAppName=${appName/$SPARK/$GERENUK}
    else
	gerenukAppName="${GERENUK}${appName}"
    fi
    sameOutputFile="/dataForSparkGerenuk/outputs/outputFile"
    sparkOutputFile="$sameOutputFile" #"/dataForSparkGerenuk/outputs/$appName"
    gerenukOutputFile="$sameOutputFile" #"/dataForSparkGerenuk/outputs/$gerenukAppName"	
    sparkClass="org.apache.spark.examples"
    gerenukClass="org.apache.spark.examples"    
    if [[ "$appName" == "ConnectedComponents" || "$appName" == "TriangleCounting" ]]; then
	sparkClass="${sparkClass}.graphx.${appName}"
	gerenukClass="${gerenukClass}.graphx.${gerenukAppName}"
    else
	sparkClass="${sparkClass}.${appName}"
	gerenukClass="${gerenukClass}.${gerenukAppName}"
    fi
    cmdCommons="--deploy-mode cluster --master yarn --num-executors $num_executors --executor-cores $driverCores  --driver-cores $driverCores"
    echo "===$appName===" >> $timeFile
    for heapSize in ${heapSizeList[@]}
    do
	hdfs dfs -rm -r $sparkOutputFile
	hdfs dfs -rm -r $sparkOutputFile
	hdfs dfs -rm -r $sparkOutputFile
	sparkCmd="timeout 4h ./bin/spark-submit  --name \"${appName}_$heapSize\" $cmdCommons  --driver-memory $heapSize --executor-memory $heapSize --class $sparkClass  $sparkJar $input $sparkOutputFile |& tee ./outputSpark.txt"
	echo -n "$heapSize:" >> $timeFile
	wholeCmd="{ time $sparkCmd ; } 2>> $timeFile"
	#echo "$wholeCmd"
        eval "$wholeCmd"
    done
    echo  "===$gerenukAppName===" >> $timeFile
    for heapSize in ${heapSizeList[@]}
    do
	hdfs dfs -rm -r $gerenukOutputFile
	hdfs dfs -rm -r $sparkOutputFile
	hdfs dfs -rm -r $sparkOutputFile
	gerenukCmd="timeout 4h ./bin/spark-submit  --name \"${gerenukAppName}_$heapSize\" $cmdCommons  --driver-memory $heapSize --executor-memory $heapSize --class $gerenukClass $sparkJar $input $gerenukOutputFile |& tee ./outputGerenuk.txt"
	echo -n "$heapSize:" >> $timeFile
	wholeCmd="{ time $gerenukCmd ; } 2>> $timeFile"
	#echo "$wholeCmd"
	eval "$wholeCmd"
    done
done

echo "Finished!"
